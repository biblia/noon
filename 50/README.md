# Mga Taga Filipos

|reference     | start | end |
|:---------------|----:|----:|
| Filipos 1:1-30 |    [1](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L1)|   30|
| Filipos 2:1-30 |   [31](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L31)|   60|
| Filipos 3:1-21 |   [61](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L61)|   81|
| Filipos 4:1-23 |   [82](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L82)|  104|

#### source of [master](https://gitlab.com/biblia/noon/master/50/Php/README.md): Diglot

* base is copied from Tagalog/English Old Version Bible Diglot [TAG/KJV 055] PBS 2011 ISBN 978-971-29-0226-0

### From [ABTAG01](https://www.bible.com/tl/bible/2195/)

###### Pagbati [Filipos 1:1-2](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L1)

###### Panalangin ni Pablo para sa mga Taga-Filipos [Filipos 1:3-11](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L3)

###### Ang Mabuhay ay si Cristo [Filipos 1:12-30](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L12)

###### Tularan ang Pagpapakumbaba ni Cristo [Filipos 2:1-11](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L31)

###### Magsilbing Ilaw ng Sanlibutan [Filipos 2:12-18](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L42)

###### Sina Timoteo at Epafrodito [Filipos 2:19-30](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#L42)

###### Paghiwalay sa Nakaraan [Filipos 3:1-11](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#61)

###### Pagpapatuloy ng Mithiin [Filipos 3:12-21—4:1](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#72)

###### Mga Pangaral [Filipos 4:2-9](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#83)

###### Pasasalamat sa Kaloob ng mga Taga-Filipos [Filipos 4:10-20](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#91)

###### Mga Pagbati at Basbas [Filipos 4:21-23](https://gitlab.com/biblia/noon/blame/master/50/Php/README.md#102)
