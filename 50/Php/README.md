Si Pablo at si Timoteo, na mga alipin ni Cristo Jesus, sa lahat ng mga banal kay Cristo Jesus na nangasa Filipos, pati ng mga obispo at ng mga diakono:
Sumainyo nawa ang biyaya at kapayapaang mula sa Dios na ating Ama at sa Panginoong Jesucristo. 
Ako'y nagpapasalamat sa aking Dios, sa tuwing kayo'y aking naaalala,
Na parating sa bawa't daing ko, ay masayang nananaig ako na patungkol sa inyong lahat,
Dahil sa inyong pakikisama sa pagpapalaganap ng evangelio, mula nang unang araw hanggang ngayon;
Na ako'y may lubos na pagkakatiwala sa bagay na ito, na ang nagpasiula sa inyo ng mabuting gawa, ay lulubusin hanggang sa araw ni Jesucristo:
Gaya ng matuwid na aking isiping gayon tungkol sa inyong lahat, sapagka't kayo'y nasa aking puso, palibhasa'y, sa aking mga tanikala at pagsasanggalang at sa pagpapatunay naman sa evangelio, kayong lahat na kasama ko ay may bahagi sa biyaya.
Sapagka't saksi ko ang Dios, kung gaano ang pananabik ko sa inyong lahat sa mahinahong habag ni Cristo Jesus.
At ito'y idinadalangin ko, na ang inyong pagibig ay lalo'y lalo pang sumagana nawa sa kaalaman at sa lahat ng pagkakilala;
Upang inyong kilalanin ang mga bagay na magagaling; upang kayo'y maging mga tapat at walang kapintasan hanggang sa kaarawan ni Cristo;
Na mangapuspos ng bunga ng kabanalan, na ito'y sa pamamagitan ni Jesucristo, sa ikaluluwalhati at ikapupuri ng Dios.
Ngayon ibig ko na inyong maalaman, mga kapatid, na ang mga bagay _na nangyayari_ sa akin ay nangyari sa lalong ikasusulong ng evangelio;
Ano pa't ang aking mga tanikala kay Cristo ay nahayag sa lahat ng mga bantay ng pretorio, at sa mga iba't iba pa;
At ang karamihan sa mga kapatid sa Panginoon, na palibhasa'y may pagkakatiwala sa aking mga tanikala, ay lalong nagkaroon ng tapang upang salitaing walang takot ang salita ng Dios.
Tunay na ipinangangaral ng iba si Cristo sa kapanaghilian at sa pakikipagtalo; at ng mga iba naman sa mabuting kalooban:
Ang isa'y _gumagawa nito_ sa pagibig, palibhasa'y nalalaman na ako'y nalalagay sa pagsasanggalang ng evangelio;
Datapuwa't itinatanyag ng iba si Cristo dahil sa pagkakampikampi, hindi sa pagtatapat, na ang iniisip ay dalhan ako ng kapighatian sa aking mga tanikala.
Ano nga? gayon man, sa lahat ng paraan, maging sa pagdadahilan o sa katotohanan, ay itinatanyag si Cristo; at sa ganito'y nagagalak ako, oo, at ako'y magagalak.
Sapagka't nalalaman ko na ang kahihinatnan nito'y sa aking ikaliligtas, sa pamamagitan ng inyong pananaing at kapuspusan ng Espiritu ni Cristo,
Ayon sa aking maningas na paghihintay at pagasa, na, sa anoma'y hindi ako mapapahiya, kundi sa buong katapangan, na gaya ng dati, gayon din naman ngayon, ay dadakilain si Cristo sa aking katawan, maging sa pamamagitan ng kabuhayan, o sa pamamagitan ng kamatayan.
Sapagka't sa ganang akin ang mabuhay ay si Cristo, at ang mamatay ay pakinabang.
Nguni't kung ang mabuhay sa laman _ay siya kong palad,_—ito'y magiging mabungang pagpapagal, na aywan ko nga kung ano ang aking pipiliin.
Sapagka't ako'y nagigipit sa magkabila, akong may nasang umalis at suma kay Cristo; sapagka't ito'y lalong mabuti:
Gayon ma'y ang manatili sa laman ay siyang lalong kinakailangan dahil sa inyo.
At sa pagkakatiwalang ito, ay aking nalalaman na ako'y mananatili, oo, at mananatili ako na kasama ninyong lahat, sa ikasusulong ninyo at ikagagalak sa pananampalataya;
Upang managana ang inyong pagmamapuri kay Cristo Jesus sa akin sa pamamagitan ng aking pagharap na muli sa inyo.
Ang inyo lamang pamumuhay ay maging karapatdapat sa evangelio ni Cristo: upang, maging ako ay dumating at kayo'y makita, o wala man sa harap ninyo, ay mabalitaan ko ang inyong kalagayan, na kayo'y matitibay sa isang espiritu, na kayo'y mangagkaisa ng kaluluwa na nangagsisikap sa pananampalataya sa evangelio;
At sa anoman ay huwag kayong mangatakot sa mga kaaway: na ito sa kanila ay malinaw na tanda ng kapahamakan, datapuwa't _tanda_ ng inyong pagkaligtas, at ito'y mula sa Dios;
Sapagka't sa inyo'y ipinagkaloob alangalang kay Cristo, hindi lamang upang manampalataya sa kaniya, kundi upang magtiis din naman alangalang sa kaniya:
Yamang taglay ninyo ang pakikipagbuno na inyong nakita rin sa akin, at ngayo'y nababalitaan ninyong taglay ko.
Kaya nga kung mayroong anomang kasiglahan kay Cristo, kung _mayroong_ anomang pakikisama ng Espiritu, kung _mayroong_ anomang mahinahong awa at habag,
Ay lubusin ninyo ang aking katuwaan, upang kayo'y mangagkaisa ng pagiisip, mangagtaglay ng isa ring pagibig, na mangagkaisa ng akala, _at_ isa lamang pagiisip;
Na huwag ninyong _gawin_ ang anoman sa pamamagitan ng pagkakampikampi o sa pamamagitan ng pagpapalalo, kundi sa kababaan ng pagiisip, na ipalagay ng bawa't isa ang iba na lalong mabuti kay sa kaniyang sarili;
Huwag tingnan ng bawa't isa sa inyo ang sa kaniyang sarili, kundi ang bawa't isa naman ay sa iba't iba.
Mangagkaroon kayo sa inyong pagiisip, na ito'y na kay Cristo Jesus din naman:
Na siya, _bagama't_ nasa anyong Dios, ay hindi niya inaring isang bagay na nararapat panangnan ang pagkapantay _niya_ sa Dios,
Kundi bagkus hinubad niya ito, at naganyong alipin, na nakitulad sa mga tao:
At palibhasa'y nasumpungan sa anyong tao, siya'y nagpakababa sa kaniyang sarili, na nagmasunurin hanggang sa kamatayan, oo, sa kamatayan sa krus.
Kaya sila naman ay pinakadakila ng Dios, at siya'y binigyan ng pangalang lalo sa lahat ng pangalan;
Upang sa pangalan ni Jesus ay iluhod ang lahat ng tuhod, ng nangasa langit, at ng nangasa ibabaw ng lupa, at ng nangasa ilalim ng lupa,
At upang ipahayag ng lahat ng mga dila na si Jesucristo ay Panginoon, sa ikaluluwalhati ng Dios Ama.
Kaya nga, mga minamahal ko, kung paano ang inyong laging pagsunod, na hindi lamang sa harapan ko, kundi bagkus pa ngayong ako'y wala, ay lubusin ninyo ang gawain ng inyong sariling pagkaligtas na may takot at panginginig;
Sapagka't Dios ang gumagawa sa inyo maging sa pagnanasa at sa paggawa, ayon sa kaniyang mabuting kalooban.
Gawin ninyo ang lahat ng mga bagay na walang mga bulungbulong at pagtatalo:
Upang kayo'y maging walang sala at walang malay, mga anak ng Dios na walang dungis sa gitna ng isang lahing liko at masama, na sa gitna nila'y lumiliwanag kayong tulad sa mga ilaw sa sanglibutan,
Na nagpapahayag ng salita ng kabuhayan; upang may ipagkapuri ako sa kaarawan ni Cristo, na hindi ako tumakbo nang walang kabuluhan ni nagpagal man nang walang kabuluhan.
Oo, kahit ako'y maging hain sa paghahandog at paglilingkod ng inyong pananampalataya, ako'y nakikipagkatuwa, at nakikigalak sa inyong lahat:
At sa ganyan ding paraan kayo'y nakikipagkatuwa naman, at nakikigalak sa akin.
Datapuwa't inaasahan ko sa Panginoong Jesus na suguing madali sa inyo si Timoteo, upang ako naman ay mapanatag, pagkaalam ko ng inyong kalagayan.
Sapagka't walang taong katulad ko ang pagiisip na magmamalakasakit na totoo sa inyong kalagayan.
Sapagka't pinagsisikapan nilang lahat ang sa kanilang sarili, hindi ang mga bagay ni Jesucristo.
Nguni't nalalaman ninyo ang pagkasubok sa kaniya na gaya ng _paglilingkod_ ng anak sa ama, _ay gayon_ naglilingkod siyang kasama ko sa ikalalaganap ng evangelio.
Siya nga ang aking inaasahang suguin madali, pagkakita ko kung ano ang mangyayari sa akin:
Datapuwa't umaasa ako sa Panginoon, na diya'y makararating din naman akong madali.
Nguni't inaakala kong kailangang suguin sa inyo si Epafrodito, na aking kapatid at kamanggagawa, at kapuwa kawal at inyong sugo at katiwala sa aking kailangan.
Yamang sila'y nananabik sa inyong lahat, at totoong siya'y namanglaw, sapagka't inyong nabalitaan na siya'y may-sakit:
Katotohanan ngang nagkasakit siya na malapit na sa kamatayan: nguni't kinahabagan siya ng Dios; at hindi lamang siya kundi pati ako, upang ako'y huwag magkaroon ng sapinsaping kalumbayan.
Siya nga'y sinugo kong may malaking pagpipilit, upang, pagkakitang muli ninyo sa kaniya, kayo'y mangagalak, at ako'y mabawasan ng kalumbayan.
Tanggapin nga ninyo siya sa Panginoon ng buong galak; at ang gayon ay papurihan ninyo:
Sapagka't dahil sa pagpapagal kay Cristo ay nalapit siya sa kamatayan, na isinasapanganib ang kaniyang buhay upang punan ang kakulangan sa inyong paglilingkod sa akin.
Sa katapustapusan, mga kapatid ko, mangagalak kayo sa Panginoon. Ang pagkasulat ko nga sa inyo ng mga gayon ding bagay, sa akin ay tunay na hindi nakangangalay, nguni't sa inyo'y katiwasayan.
Magsipagingat kayo sa mga aso, magsipagingat kayo sa masasamang manggagawa, magsipagingat kayo sa mga sa pagtutuli:
Sapagka't tayo ang pagtutuli, na nagsisisamba sa Espiritu ng Dios, at nangagmamapuri kay Cristo Jesus, at walang anomang pagkakatiwala sa laman:
Bagama't ako'y makapagkakatiwala sa laman: na kung ang iba ay nagaakala na may pagkakatiwala sa laman, ay lalo na ako:
Na tinuli ng ikawalong araw, mula sa lahi ng Israel, mula sa angkan ni Benjamin, Hebreo sa mga Hebreo; tungkol sa kautusan, ay Fariseo;
Tungkol sa pagsisikap, ay manguusig sa iglesia; tungkol sa kabanalan na nasa kautusan, ay walang kapintasan.
Gayon man ang mga bagay na sa akin ay pakinabang, ay inari kong kalugihan, alangalang kay Cristo.
Oo nga, at lahat ng mga bagay ay inaari kong kalugihan dahil sa dakilang kagalingan ng pagkakilala kay Cristo Jesus na Panginoon ko: na alangalang sa kaniya'y tiniis ko ang kalugihan ng lahat ng mga bagay, at inari kong sukal laman, upang tamuhin ko si Cristo,
At ako'y masumpungan sa kaniya, na walang katuwirang aking sarili, _sa makatuwid baga'y_ sa kautusan, kundi ang katuwirang sa pamamagitan ng pananampalataya kay Cristo, ang katuwiran ngang buhat sa Dios sa pamamagitan ng pananampalataya:
Upang makilala ko siya, at ang kapangyarihan  ng kaniyang mga kahirapan, na _ako'y_ natutulad sa kaniyang pagkamatay;
Kung aking tamuhin sa anomang paraan ang pagkabuhay na maguli sa mga patay.
Hindi sa ako'y nagtamo na, o ako'y nalubos na: kundi nagpapatuloy ako, baka sakaling maabot ko yaong ikinaaabot naman sa akin ni Cristo Jesus.
Mga kapatid, hindi ko pa inaaring inabot: datapuwa't isang bagay ang _ginagawa ko_, na nililimot ang mga dating bagay na nasa likuran, at tinutungo ang mga bagay na hinaharap,
Nagtutumulin ako sa hangganan sa ganting-pala ng dakilang pagtawag ng Dios na kay Cristo Jesus.
Kaya nga, kung ilan tayong mga sakdal, ay magisip ng gayon: at kung sa anoma'y nangagkakaiba kayo ng iniisip ay ipahahayag naman ito sa inyo ng Dios:
Lamang, ay magsilakad tayo ayon sa gayon ding _ayos_ na ating inabot na.
Mga kapatid, kayo'y mangagkaisang tumulad sa akin, at tandaan ninyo ang mga nagsisilakad ng gayon, ayon sa halimbawang _nakikita_ ninyo sa akin.
Sapagka't marami ang mga nagsisilakad ng gayon, na siyang madalas na aking sinabi sa inyo, at ngayo'y sinasabi ko sa inyo na may pagiyak, _na sila_ ang mga kaaway ng krus ni Cristo:
Na ang kanilang kahihinatnan ay ang kapahamakan, na ang kanilang dios ay ang tiyan, at _ang kanilang_ kapurihan ay nasa kanilang kahihiyan, na nagiisip ng mga bagay na ukol sa lupa.
Sapagka't ang ating pagkamamamayan ay nasa langit; mula doon ay hinihintay naman natin ang Tagapagligtas, ang Panginoong Jesucristo:
Na siyang magbabago ng katawan ng ating pagkamababa, _upang maging_ katulad ng katawan ng kaniyang kaluwalhatian, ayon sa paggawa na maipagpapasuko niya sa lahat ng mga bagay sa kaniya.
Kaya nga, mga kapatid kong minamahal at pinananabikan, aking katuwaan at putong, magsitibay nga kayo sa Panginoon, mga minamahal ko.
Ipinamamanhik ko kay Euodias, at ipinamamanhik ko kay Sintique, na mangagkaisa ng pagiisip sa Panginoon.
Oo, ipinamamanhik ko rin naman sa iyo, tapat na kasama sa pagtulong, na iyong tulungan ang mga babaeng ito, sapagka't sila'y nangagpagal na kasama ko sa evangelio, at kasama rin naman ni Clemente, at ng ibang aking mga kamanggagawa, na ang kanilang pangalan ay nangasa aklat ng buhay.
Mangagalak kayong lagi sa Panginoon: muli kong sasabihin, Mangagalak kayo.
Makilala nawa ang inyong kahinhinan ng lahat ng mga tao. Ang Panginoon ay malapit na.
Huwag kayong mangabalisa sa anomang bagay; kundi sa lahat ng mga bagay sa pamamagitan ng panalangin at daing na may pagpapasalamat ay ipakilala ninyo ang inyong mga kahilingan sa Dios.
At ang kapayapaan ng Dios, na di masayod ng pagiisip, ay magiingat ng inyong mga puso at ng inyong mga pagiisip kay Cristo Jesus.
Katapustapusan, mga kapatid, anomang bagay na katotohanan, anomang bagay na kagalanggalang, anomang bagay na matuwid, anomang bagay na malinis, anomang bagay na kaibigibig, anomang bagay na mabuting ulat; kung may anomang kagalingan, at kung may anomang kapurihan, ay isipin ninyo ang mga bagay na ito.
Ang mga bagay na inyong natutuhan at tinanggap at narinig at nakita sa akin, ang mga bagay na ito ang gawin ninyo: at ang Dios ng kapayapaan ay sasa inyo.
Datapuwa't ako'y totoong nagagalak sa Panginoon, na ngayon sa kahulihulihan ay inyong binuhay ang inyong pagmamalasakit sa akin; na dito'y katotohanang nangagkaroon kayo ng malasakit, nguni't kayo'y nagkulang ng mabuting pagkakataon.
Hindi sa sinasabi ko ang tungkol sa kailangan: sapagka't aking natutuhan ang masiyahan sa anomang kalagayang aking kinaroroonan.
Marunong akong magpakababa, at marunong naman akong magpakasagana: sa bawa't bagay at sa lahat ng bagay ay natutuhan ko ang lihim maging sa kabusugan, at maging sa kagutuman, at maging sa kasaganaan at maging sa kasalatan.
Lahat ng mga bagay ay aking magagawa doon sa nagpapalakas sa akin.
Gayon man ay mabuti ang inyong ginawa na kayo'y nakiramay sa aking kapighatian.
At kayo mga taga Filipos, nalalaman naman ninyo, na nang pasimulan ang evangelio, nang ako'y umalis sa Macedonia, alin mang iglesia ay walang nakipagkaisa sa akin sa pagkakaloob at pagtanggap kundi kayo laman;
Sapagka't sa Tesalonica ay nagpadala kayong minsan at muli para sa aking kailangan.
Hindi sa ako'y naghahanap ng kaloob; kundi hinahanap ko ang bunga na dumadami sa ganang inyo.
Datapuwa't mayroon ako ng lahat ng mga bagay, at sumasagana: ako'y busog, palibhasa'y tumanggap kay Epafrodito ng mga bagay _na galing_ sa inyo, na isang samyo ng masarap na amoy, isang handog na kaayaaya, na lubhang nakalulugod sa Dios.
At pupunan ng aking Dios ang bawa't kailangan ninyo ayon sa kaniyang mga kayamanan sa kaluwalhatian kay Cristo Jesus.
Ngayon _nawa'y_ suma ating Dios at Ama ang kaluwalhatian magpakailan man. Siya nawa.
Batiin ninyo ang bawa't banal kay Cristo Jesus. Binabati kayo ng mga kapatid na kasama ko.
Binabati kayo ng lahat ng mga banal, lalong lalo na ng mga kasangbahay ni Cesar.
Ang biyaya ng Panginoong Jesucristo ay sumainyo nawang espiritu.