# 2 Mga Taga Corinto

|reference     | start | end |
|:---------------|----:|----:|
|2 Corinto 1:1-24|    1|   24|
|2 Corinto 2:1-17|   25|   41|
|2 Corinto 3:1-18|   42|   59|
|2 Corinto 4:1-18|   60|   77|
|2 Corinto 5:1-21|   78|   98|
|2 Corinto 6:1-18|   99|  116|
|2 Corinto 7:1-16|  117|  132|
|2 Corinto 8:1-24|  133|  156|
|2 Corinto 9:1-15|  157|  171|
|2 Corinto 10:1-18| 172|  189|
|2 Corinto 11:1-33| 190|  222|
|2 Corinto 12:1-21| 223|  243|
|2 Corinto 13:1-14| 244|  257|
