Ang matanda sa hirang na ginang at sa kaniyang mga anak, na aking iniibig sa katotohanan; at hindi lamang ako, kundi pati ng lahat ng mga nakakakilala ng katotohanan;
Alangalang sa katotohanan na nananahan sa atin, at sasa atin magpakailan man:
Sumaatin nawa ang biyaya, awa, _at_ kapayapaang mula sa Dios Ama at kay Jesucristo, Anak ng Ama, sa katotohanan at sa pagibig.
Ako'y lubhang nagagalak na aking nasumpungan ang _ilan_ sa iyong mga anak na nagsisilakad sa katotohanan, ayon sa ating tinanggap na utos sa Ama.
At ngayo'y ipinamamanhik ko sa iyo, ginang, na hindi waring sinusulatan kita ng isang bagong utos, kundi niyaong ating tinanggap nang pasimula, na tayo'y mangagibigan sa isa't isa.
At ito ang pagibig, na tayo'y mangagsilakad ayon sa kaniyang mga utos. Ito ang utos, na tayo'y mangagsilakad sa kaniya, gaya ng inyong narinig nang pasimula.
Sapagka't maraming magdaraya na nangagsilitaw sa sanglibutan, _sa makatuwid ay_ ang mga hindi nangagpapahayag na si Jesucristo ay napariritong nasa laman. Ito ang magdaraya at ang anticristo.
Mangagingat kayo sa inyong sarili, upang huwag ninyong iwala ang mga bagay na aming pinagpagalan, kundi upang tanggapin ninyo ang isang lubos na kagantihan.
Ang sinomang nagpapatuloy at hindi nananahan sa aral ni Cristo, ay hindi kinaroroonan ng Dios: ang nananahan sa aral, ay kinaroroonan ng Ama at gayon din ng Anak.
Kung sa inyo'y dumating ang sinoman, at hindi dala ang aral na ito, ay huwag ninyong tanggapin sa inyong bahay, at huwag ninyo siyang batiin:
Sapagka't ang bumabati sa kaniya ay nararamay sa kaniyang masasamang gawa.
Yamang may maraming mga bagay na isusulat sa inyo, ay hindi ko ibig _isulat_ sa papel at tinta; datapuwa't inaasahan kong pumariyan sa inyo, at makipagusap ng mukhaan, upang malubos ang inyong galak.
Ang mga anak ng iyong hirang na kapatid na babae ay bumabati sa iyo.