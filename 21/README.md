# Eclesiastes
Bagaman hinati hati, ito'y iisang aklat lamang.

##### Katunayan
Dito ay magkahiwalay ang Eclesiastes 7:29 at Eclesiastes 8:1, sa dalawang kabanata; maging sa Diglot ay nasa dalawang bahagi rin: nasa `Ang kagamitan ng karunungan at ang kahangalan ng kasamaan.` at nasa `Ang mga namumuno ay dapat na igalang.` ang dalawang talatang ito; subalit, magkasama sa iisang talata na Eclesiastes 7:30 sa Douay-Rheims 1899 (Vulg) ng [Omega Digibible](https://omegadigibible.com).

Gaya po ng aming natutunan sa [Ang Dating Daan](https://www.facebook.com/AngDatingDaanTV). Maaari po natin itong abangan sa kanilang Bible Study o maaaring itanong ninoman sa kanila ring Bible Exposition.

|reference     | start | end |
|:---------------|----:|----:|
| Eclesiastes 1:1-18 |    [1](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L1)|   18|
| Eclesiastes 2:1-26 |   [19](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L19)|   44|
| Eclesiastes 3:1-22 |   [45](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L45)|   66|
| Eclesiastes 4:1-16 |   [67](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L67)|   82|
| Eclesiastes 5:1-20 |   [83](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L83)|  102|
| Eclesiastes 6:1-12 |  [103](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L103)|  114|
| Eclesiastes 7:1-29 |  [115](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L115)|  143|
| Eclesiastes 8:1-17 |  [144](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L144)|  160|
| Eclesiastes 9:1-18 |  [161](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L161)|  178|
| Eclesiastes 10:1-20 | [179](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L179)|  198|
| Eclesiastes 11:1-10 | [199](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L199)|  208|
| Eclesiastes 12:1-14 | [209](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L209)|  222|

_Tingnan ang #1_

###### Ang mangangaral ay nagsasabing ang lahat ng bagay ay walang kabuluhan. [Eclesiastes 1:1-18](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L1)

###### Ang pagkawalang kabuluhan ng tinatangkilik, karunungan, at ng lahat na gawa. [Eclesiastes 2:1-26—3:1-15](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L19)

###### Ang kasamaan ng tao at ang pagkakatulad sa hayop. [Eclesiastes 3:16-22—4:1-16](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L60)

###### Ang kabanalan ay ipinayo. Maling paggamit ng kayamanan. [Eclesiastes 5:1-20](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L83)

###### Ang kawalang kabuluhan ng mabuting bagay na hindi ikasya. [Eclesiastes 6:1-12](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L103)

###### Ang kagamitan ng karunungan at ang kahangalan ng kasamaan. [Eclesiastes 7:1-29](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L115)

###### Ang mga namumuno ay dapat na igalang. [Eclesiastes 8:1-8](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L144)

###### Ang hindi pagkakapantaypantay ng buhay. [Eclesiastes 8:9-15](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L152)

###### Ang hiwaga ng mga gawa ng Panginoon. [Eclesiastes 8:16-17—9:1-10](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L159)

###### Ang pangyayari sa buhay ay hindi tiyak. [Eclesiastes 9:11-12](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L171)

###### Ang kadakilaan ng karunungan. [Eclesiastes 9:13-18](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L173)

###### Ang pagkakaawang gawa at ang kasipagan ay dapat gawin na may pagasa. [Eclesiastes 10:1-20—11:1-10](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L179)

###### Payo upang alalahanin ang Panginoon sa kabataan. [Eclesiastes 12:1-14](https://gitlab.com/biblia/noon/blame/master/21/Ecc/README.md#L209)
