# Mga Taga Galacia

|reference     | start | end |
|:---------------|----:|----:|
| Galacia 1:1-24 |    1|   24|
| Galacia 2:1-21 |   25|   45|
| Galacia 3:1-29 |   46|   74|
| Galacia 4:1-31 |   75|  105|
| Galacia 5:1-26 |  106|  131|
| Galacia 6:1-18 |  132|  149|