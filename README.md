# What is this?
This is an attempt to restore the original Bible. How? By removing chapter divisions.
The Bible is originally a compilation of $`\beta\iota\beta\lambda\iota\alpha`$ or scrolls.
For easier citations, a priest divided them into chapters and later into verses.
However, some sentences were separated into different verses, and some, disappointingly, into different chapters.

# Updates
There is already a group that retranslates the Bible in conformity with its context. Your Philippine Institution 100 (PI 100 in UP) teacher would agree that context is important in understanding texts if our intention is understanding the author / authorities of the works.
That is the Ang Dating Biblia, or in English, the old Bible. I will only copy the changes the Ang Dating Biblia and will not get ahead of them. This is only my personal copy of the changes made in Ang Dating Biblia.

## Hebrew name of God
יהוה
