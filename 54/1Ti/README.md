Si Pablo, na apostol ni Cristo Jesus ayon sa utos ng Dios na ating Tagapagligtas, at ni Cristo Jesus na ating pagasa;
Kay Timoteo na aking tunay na anak sa pananampalataya: Biyaya, kahabagan, at kapayapaan nawang mula sa Dios na Ama at kay Jesucristo na Panginoon natin.
Kung paanong ipinamamanhik ko sa iyo na ikaw ay matira sa Efeso, nang pumaparoon ako sa Macedonia, upang maipagbilin mo sa ilang tao na huwag magsipagturo ng ibang aral,
Ni huwag makinig sa mga katha at sa mga kasaysayan ng mga lahi na walang katapusan, na pinanggagalingan ng pagtatalo, at hindi ng pagkakatiwalang mula sa Dios na nasa pananampalataya; ay gayon din ang ipinamamanhik ko ngayon.
Nguni't ang kinauuwian ng bilin ay ang pagibig na nagbubuhat sa malinis na puso at sa mabuting budhi at sa pananampalatayang hindi paimbabaw:
Na pagkasinsay ng iba sa mga bagay na ito ay nagsibaling sa walang kabuluhang pananalita;
Na nagsisipagnasang maging mga guro ng kautusan, bagaman di nila natatalastas kahit ang kanilang sinasabi, kahit ang kanilang buong tiwalang pinatutunayan.
Datapuwa't nalalaman natin na ang kautusan ay mabuti, kung ginagamit ng tao sa matuwid,
Yamang nalalaman ito, na ang kautusan ay hindi ginawa dahil sa taong matuwid, kundi sa mga walang kautusan at manggugulo, dahil sa masasama at mga makasalanan, dahil sa mga di banal at mapaglapastangan, dahil sa nagsisipatay sa ama at sa nagsisipatay sa ina, dahil sa mga mamamatay-tao,
Dahil sa mga nakikiapid, dahil sa mga mapakiapid sa kapuwa lalake, dahil sa mga nagnanakaw ng tao, dahil sa mga bulaan, dahil sa mga mapagsumpa ng kabulaanan, at kung mayroon pang ibang bagay laban sa mabuting aral;
Ayon sa evangelio, ng kaluwalhatian ng mapagpalang Dios, na ipinagkatiwala sa akin.
Nagpapasalamat ako sa kaniya na nagpapalakas sa akin, kay Cristo Jesus na Panginoon natin, sapagka't ako'y inari niyang tapat, na ako'y inilagay sa paglilingkod sa kaniya;
Bagaman nang una ako'y naging mamumusong, at manguusig; at mangaalipusta: gayon ma'y kinahabagan ako, sapagka't yao'y ginawa ko sa di pagkaalam sa kawalan ng pananampalataya;
At totoong sumagana ang biyaya ng ating Panginoon na nasa pananampalataya at pagibig na pawang kay Cristo Jesus.
Tapat ang pasabi, at nararapat tanggapin ng lahat, na si Cristo Jesus ay naparito sa sanglibutan upang iligtas ang mga makasalanan; na ako ang pangulo sa mga ito;
Gayon ma'y dahil dito, kinahabagan ako, upang sa akin na pangulong makasalanan, ay maipahayag ni Jesucristo ang buong pagpapahinuhod niya, na halimbawa sa mga magsisisampalataya sa kaniya, sa ikabubuhay na walang hanggan.
Ngayon sa Haring walang hanggan, walang kamatayan, di nakikita, sa iisang Dios, ay ang kapurihan at kaluwalhatian magpakailan kailan man. Siya nawa.
Ang biling ito ay ipinagtatagubilin ko sa iyo, Timoteo na aking anak, ayon sa mga hula na nangauna tungkol sa iyo, upang sa pamamagitan ng mga ito ay makipagbaka ka ng mabuting pakikipagbaka;
Na ingatan mo ang pananampalataya at ang mabuting budhi; na nang ito'y itakwil ng iba sa kanila ay nangabagbag tungkol sa pananampalataya:
Na sa mga ito'y si Himeneo at si Alejandro; na sila'y aking ibinigay kay Satanas, upang sila'y maturuang huwag mamusong.
Una-una nga sa lahat ng mga bagay ay iniaral ko na manaing, manalangin, mamagitan, at magpasalamat na patungkol sa lahat ng mga tao;
Ang mga hari at ang lahat ng nangasa mataas na kalagayan; upang tayo'y mangabuhay na tahimik at payapa sa buong kabanalan at kahusayan.
Ito'y mabuti at nakalulugod sa paningin ng Dios na ating Tagapagligtas;
Na siyang may ibig na ang lahat ng mga tao'y mangaligtas, at mangakaalam ng katotohanan.