# 1 Kay Timoteo

|reference     | start | end |
|:---------------|----:|----:|
| 1 Timoteo 1:1-20 |    [1](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L1)|   20|
| 1 Timoteo 2:1-15 |   [21](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L21)|   35|
| 1 Timoteo 3:1-16 |   [36](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L36)|   51|
| 1 Timoteo 4:1-16 |   [52](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L52)|   67|
| 1 Timoteo 5:1-25 |   [68](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L82)|   92|
| 1 Timoteo 6:1-21 |   [93](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L82)|  113|

#### source of [master](https://gitlab.com/biblia/noon/master/54/1Ti/README.md): Diglot

* master is based on Omega Digibible

### From [ABTAG01](https://www.bible.com/tl/bible/2195/)

###### Pagbati [1 Timoteo 1:1-2](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L1)

###### Babala Laban sa Maling Aral [1 Timoteo 1:3-11](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L3)

###### Pasasalamat sa Awa ng Diyos [1 Timoteo 1:12-20](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L12)

###### Mga Panalangin sa Kapulungan [1 Timoteo 2:1-15](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L21)

###### Mga Katangian ng Magiging Obispo [1 Timoteo 3:1-7](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L36)

###### Mga Katangian sa Pagiging Diakono [1 Timoteo 3:8-13](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#L43)

###### Ang Hiwaga ng Ating Pananampalataya [1 Timoteo 3:14-16](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#49)

###### Pagtalikod sa Pananampalataya [1 Timoteo 4:1-5](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#52)

###### Ang Mabuting Lingkod ni Cristo [1 Timoteo 4:6-16](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#57)

###### Mga Tungkulin sa mga Mananampalataya [1 Timoteo 5:1-25—6:1-2](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#68)

###### Maling Aral at ang Tunay na Kayamanan [1 Timoteo 6:3-10](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#95)

###### Ang Mabuting Pakikipaglaban [1 Timoteo 6:11-19](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#103)

###### Iba pang Tagubilin at Pagbasbas [1 Timoteo 6:20-21](https://gitlab.com/biblia/noon/blame/master/54/1Ti/README.md#112)