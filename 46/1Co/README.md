Si Pablo, na tinawag na maging apostol ni Jesucristo sa pamamagitan ng kalooban ng Dios, at si Sostenes na ating kapatid,
Sa iglesia ng Dios na nasa Corinto, sa makatuwid baga'y sa mga pinapagingbanal kay Cristo Jesus, na tinawag na mangagbanal, na kasama ng lahat ng mga nagsisitawag sa bawa't dako, sa pangalan ng ating Panginoong Jesucristo, na kanila at ating Panginoon:
Sumainyo nawa ang biyaya at kapayapaang mula sa Dios na ating Ama at sa Panginoong Jesucristo.
Nagpapasalamat akong lagi sa aking Dios tungkol sa inyo, dahil sa biyaya ng Dios na ipinagkaloob sa inyo sa pamamagitan ni Cristo Jesus;
Na kaya ay pinayaman sa kanya, sa lahat ng mga bagay sa lahat ng pananalita at sa lahat ng kaalaman;
Gaya ng pinagtibay sa inyo ang patotoo ni Cristo:
Ano pa't kayo'y hindi nagkulang sa anomang kaloob; na nagsisipaghintay ng paghahayag ng ating Panginoong Jesucristo;
Na siya namang magpapatibay sa inyo hanggang sa katapusan, upang huwag kayong mapagwikaan sa kaarawan ng ating Panginoong Jesucristo.
Ang Dios ay tapat, na sa pamamagitan niya ay tinawag kayo sa pakikisama ng kaniyang anak na si Jesucristo na Panginoon natin.
Ngayo'y ipinamamanhik ko sa inyo, mga kapatid, sa pamamagitan ng pangalan ng ating Panginoong Jesucristo, na kayong lahat ay mangagsalita ng isa lamang bagay, at huwag mangagkaroon sa inyo ng mga pagkakabahabahagi; kundi kayo'y mangalubos sa isa lamang pagiisip at isa lamang paghatol.
Sapagka't ipinatalastas sa akin tungkol sa inyo, mga kapatid ko, ng mga kasangbahay ni Cloe, na sa inyo'y may mga pagtatalotalo.
Ibig ko ngang sabihin ito, na ang bawa't isa sa inyo ay nagsasabi, Ako'y kay Pablo; at ako'y kay Apolos; at ako'y kay Cefas; at ako'y kay Cristo.
Nabahagi baga si Cristo? ipinako baga sa krus si Pablo dahil sa inyo? o binautismuhan baga kayo sa pangalan ni Pablo?
Nagpapasalamat ako sa Dios na hindi ko binautismuhan ang sinoman sa inyo, maliban si Crispo at si Gayo;
Baka masabi ninoman na kayo'y binautismuhan sa pangalan ko.
At binautismuhan ko rin naman ang sangbahayan ni Estefanas: maliban sa mga ito, di ko maalaman kung may nabautismuhan akong iba pa.
Sapagka't hindi ako sinugo ni Cristo upang bumautismo, kundi upang mangaral ng evangelio: hindi sa karunungan ng mga salita baka mawalan ng kabuluhan ang krus ni Cristo.
Sapagka't ang salita ng krus ay kamangmangan sa kanila na nangapapahamak; nguni't ito'y kapangyarihan ng Dios sa atin na nangaliligtas.
Sapagka't nasusulat, Iwawalat ko ang karunungan ng marurunong, At isasawala ko ang kabaitan ng mababait.
Saan naroon ang marunong? saan naroon ang eskriba? saan naroon ang mapagmatuwid sa sanglibutang ito? hindi baga ginawa ng Dios na kamangmangan ang karunungan ng sanglibutan?
Sapagka't yamang sa karunungan ng Dios ay hindi nakilala ng sanglibutan ang Dios sa pamamagitan ng kaniyang karunungan, ay kinalugdan ng Dios na iligtas ang mga nagsisipanampalataya sa pamamagitan ng kamangmangan ng pangangaral.
Ang mga Judio nga ay nagsisihingi ng mga tanda, at ang mga Griego ay nagsisihanap ng karunungan:
Datapuwa't ang aming ipinangangaral ay ang Cristo na napako sa krus, na sa mga Judio ay katitisuran, at sa mga Gentil ay kamangmangan;
Nguni't sa kanila na mga tinawag, maging mga Judio at mga Griego, si Cristo ang kapangyarihan ng Dios, at ang karunungan ng Dios.
Sapagka't ang kamangmangan ng Dios ay lalong marunong kay sa mga tao; at ang kahinaan ng Dios ay lalong malakas kay sa mga tao.
Sapagka't masdan ninyo ang sa inyo'y pagkatawag, mga kapatid, na hindi ang maraming marurunong ayon sa laman, hindi ang maraming may kapangyarihan, hindi ang maraming mahal na tao ang mga tinawag:
Kundi pinili ng Dios ang mga bagay na kamangmangan ng sanglibutan, upang hiyain niya ang mga marurunong; at pinili ng Dios ang mga bagay na mahihina ng sanglibutan, upang hiyain niya ang mga bagay na malalakas;
At ang mga bagay na mababa ng sanglibutan, at ang mga bagay na hinamak, ang pinili ng Dios, oo at ang mga bagay na walang halaga upang mawalang halaga ang mga bagay na mahahalaga:
Upang walang laman na magmapuri sa harapan ng Dios.
Datapuwa't sa kaniya kayo'y nangasa kay Cristo Jesus, na sa atin ay ginawang karunungang mula sa Dios, at katuwiran at kabanalan, at katubusan:
Na, ayon sa nasusulat, Ang nagmamapuri, ay magmapuri sa Panginoon.
At ako, mga kapatid, nang pumariyan sa inyo, ay hindi ako napariyan na may kagalingan sa pananalita o sa karunungan, na nagbabalita sa inyo ng patotoo ng Dios.
Sapagka't aking pinasiyahang walang maalaman anoman sa gitna ninyo, maliban na si Jesucristo, at siya na napako sa krus.
At ako'y nakisama sa inyo na may kahinaan, at may katakutan, at may lubhang panginginig.
At ang aking pananalita at ang aking pangangaral ay hindi sa mga salitang panghikayat ng karunungan, kundi sa patotoo ng Espiritu at ng kapangyarihan:
Upang ang inyong pananampalataya ay huwag masalig sa karunungan ng mga tao, kundi sa kapangyarihan ng Dios.
Gayon man, ay nangagsasalita kami ng karunungan sa mga may gulang: bagaman hindi ng karunungan ng sanglibutang ito, o ng mga may kapangyarihan sa sanglibutang ito, na ang mga ito'y nangauuwi sa wala:
Kundi sinasalita namin ang karunungan ng Dios sa hiwaga, yaong karunungan na itinalaga ng Dios, bago nilalang ang mga sanglibutan sa ikaluluwalhati natin:
Na hindi napagkilala ng sinomang pinuno sa sanglibutang ito: sapagka't kung nakilala sana nila, ay dising di ipinako sa krus ang Panginoon ng kaluwalhatian:
Datapuwa't gaya ng nasusulat, Ang mga bagay na hindi nakita ng mata, at ni narinig ng tainga, Ni hindi pumasok sa puso ng tao, Anomang mga bagay na inihanda ng Dios sa kanila na nangagsisiibig sa kaniya.
Nguni't ang mga ito ay ipinahayag sa atin ng Dios sa pamamagitan ng Espiritu; sapagka't nasisiyasat ng Espiritu ang lahat ng mga bagay, oo, ang malalalim na mga bagay ng Dios.
Sapagka't sino sa mga tao ang nakakakilala ng mga bagay ng tao, kundi ang espiritu ng tao, na nasa kaniya? gayon din naman ang mga bagay ng Dios ay hindi nakikilala ng sinoman, maliban na ng Espiritu ng Dios.
Nguni't ating tinanggap, hindi ang espiritu ng sanglibutan, kundi ang espiritung mula sa Dios; upang ating mapagkilala ang mga bagay na sa atin ay ibinigay na walang bayad ng Dios.
Na ang mga bagay na ito ay atin namang sinasalita, hindi sa mga salitang itinuturo ng karunungan ng tao, kundi sa itinuturo ng Espiritu; na iniwawangis natin ang mga bagay na ayon sa espiritu sa mga pananalitang ayon sa espiritu.
Nguni't ang taong ayon sa laman ay hindi tumatanggap ng mga bagay ng Espiritu ng Dios: sapagka't ang mga ito ay kamangmangan sa kaniya; at hindi niya nauunawa, sapagka't ang mga yaon ay sinisiyasat ayon sa espiritu.
Nguni't ang sa espiritu ay nagsisiyasat sa lahat ng mga bagay, at siya'y hindi sinisiyasat ng sinoman.
Sapagka't sino ang nakakilala ng pagiisip ng Panginoon, upang siya'y turuan niya? Datapuwa't nasa atin ang pagiisip ni Cristo.
At ako, mga kapatid, ay hindi nakapagsalita sa inyo na tulad sa mga nasa espiritu, kundi tulad sa mga nasa laman, tulad sa mga sanggol kay Cristo.
Kinandili ko kayo ng gatas, at hindi ng lamang-kati; sapagka't kayo'y wala pang kaya noon: wala, na ngayon pa man ay wala kayong kaya;
Sapagka't kayo'y mga sa laman pa: sapagka't samantalang sa inyo'y may mga paninibugho at mga pagtatalo, hindi baga kayo'y mga sa laman, at kayo'y nagsisilakad ayon sa kaugalian ng mga tao?
Sapagka't kung sinasabi ng isa, Ako'y kay Pablo; at ng iba, Ako'y kay Apolos; hindi baga kayo'y mga tao?
Ano nga si Apolos? at Ano si Pablo? Mga ministro na sa pamamagitan nila ay nagsisampalataya kayo; at sa bawa't isa ayon sa ipinagkaloob ng Panginoon sa kaniya.
Ako ang nagtanim, si Apolos ang nagdilig; nguni't ang Dios ang siyang nagpapalago.
Ano pa't walang anoman ang nagtatanim, ni ang nagdidilig; kundi ang Dios na nagpapalago.
Ngayon ang nagtatanim at ang nagdidilig ay iisa: nguni't ang bawa't isa ay tatanggap ng kaniyang sariling kagantihan ayon sa kaniyang sariling pagpapagal.
Sapagka't tayo ay mga kamanggagawa ng Dios: kayo ang bukid ng Dios, ang gusali ng Dios.
Ayon sa biyaya ng Dios na ibinigay sa akin, na tulad sa matalinong tagapagtayo ay inilagay ko ang pinagsasaligan; atiba ang nagtatayo sa ibabaw nito. Nguni't iningatan ng bawa't tao kung paano ang pagtatayo niya sa ibabaw nito.
Sapagka't sinoman ay hindi makapaglalagay ng ibang pinagsasaligan, kundi ang naglalagay na, na ito'y si Cristo Jesus.
Datapuwa't kung ang sinoma'y magtatayo sa ibabaw ng pinagsasaligang ito ng ginto, pilak, mga mahahalagang bato, kahoy, tuyong dayami;
Ang gawa ng bawa't isa ay mahahayag: sapagka't ang araw ang magsasaysay, sapagka't sa pamamagitan ng apoy ihahayag; at ang apoy rin ang susubok sa gawa ng bawa't isa kung ano yaon.
Kung ang gawa ng sinoman ay manatili, na kaniyang itinayo sa ibabaw niyaon, siya'y tatanggap ng kagantihan.
Kung ang gawa ng sinoman ay masunog, ay malulugi siya: nguni't siya sa kaniyang sarili ay maliligtas; gayon ma'y tulad sa pamamagitan ng apoy.
Hindi baga ninyo nalalaman na kayo'y templo ng Dios, at ang Espiritu ng Dios ay nananahan sa inyo?
Kung gibain ng sinoman ang templo ng Dios, siya'y igigiba ng Dios; sapagka't ang templo ng Dios ay banal, na ang templong ito ay kayo.
Sinoma'y huwag magdaya sa kaniyang sarili. Kung ang sinoman sa inyo ay nagiisip na siya'y marunong sa kapanahunang ito, ay magpakamangmang siya, upang siya ang maging marunong.
Sapagka't ang karunungan ng sanglibutang ito ay kamangmangan sa Dios. Sapagka't nasusulat, Hinuhuli niya ang marurunong sa kanilang katusuhan:
At muli, Nalalaman ng Panginoon ang pangangatuwiran ng marurunong, na pawang walang kabuluhan.
Kaya't huwag ipagmamapuri ninoman sa mga tao, Sapagka't ang lahat ng mga bagay ay sa inyo.
Kahit si Pablo, kahit si Apolos, kahit si Cefas, kahit ang sanglibutan, kahit ang buhay, kahit ang kamatayan, kahit ang mga bagay ng kasalukuyan, kahit ang mga bagay na darating; lahat ay sa inyo:
At kayo'y kay Cristo; at si Cristo ay sa Dios.
Ipalagay nga kami ng sinoman na mga ministro ni Cristo, at mga katiwala ng mga hiwaga ng Dios.
Bukod dito'y kinakailangan sa mga katiwala, na ang bawa't isa ay maging tapat.
Datapuwa't sa ganang akin ay isang bagay na totoong maliit ang ako'y siyasatin ninyo, o ng pagsisiyasat ng tao: oo, ako'y hindi nagsisiyasat sa aking sarili.
Sapagka't wala akong nalalamang anomang laban sa aking sarili; bagaman ang nagsisiyasat sa akin ay ang Panginoon.
Kaya nga huwag muna kayong magsihatol ng anoman, hanggang sa dumating ang Panginoon, na siya ang maghahayag ng mga bagay na nalilihim sa kadiliman, at ipahahayag naman ang mga haka ng mga puso; at kung magkagayon ang bawa't isa ay magkakaroon ng kapurihan sa Dios.
Ang mga bagay ngang ito, mga kapatid, ay inianyo ko sa halimbawa sa aking sarili at kay Apolos dahil sa inyo; upang sa amin ay mangatuto kayo na huwag magsihigit sa mga bagay na nangasusulat; upang ang sinoman sa inyo ay huwag magpalalo ang isa laban sa iba.
Sapagka't sino ang gumawa na ikaw ay matangi? at anong nasa iyo na hindi mo tinanggap? nguni't kung tinanggap mo, ay bakit mo ipinagmamapuri na tulad sa hindi mo tinanggap?
Kayo'y mga busog na, kayo'y mayayaman na, kayo'y nangaghari nang wala kami: at ibig ko sanang mangaghari kayo, upang kami nama'y mangagharing kasama ninyo.
Sapagka't iniisip ko, na pinalitaw ng Dios kaming mga apostol na mga kahulihulihan sa lahat, na tulad sa mga itinalaga sa kamatayan: sapagka't kami'y ginawang panoorin ng sanglibutan, at ng mga anghel, at ng mga tao.
Kami'y mga mangmang dahil kay Cristo, nguni't kayo'y marurunong kay Cristo; kami'y mahihina, nguni't kayo'y malalakas; kayo'y may karangalan, datapuwa't kami ay walang kapurihan.
Hanggang sa oras na ito'y nangagugutom kami, at nangauuhaw, at mga hubad, at mga tinampal, at wala kaming tiyak na tahanan;
At kami'y nangagpapagal, na nangagsisigawa ng aming sariling mga kamay: bagama't inuupasala, ay kami'y nangagpapala; bagama't mga pinaguusig, ay nangagtitiis kami;
Bagama't mga sinisiraangpuri, ay aming pinamamanhikan: kami'y naging tulad ng yagit sa sanglibutan, sukal ng lahat ng mga bagay hanggang ngayon.
Hindi ko isinusulat ang mga bagay na ito upang kayo'y hiyain, kundi upang paalalahanan kayong tulad sa aking mga minamahal na anak.
Sapagka't bagaman mangagkaroon kayo ng sampung libong mga guro kay Cristo, ay wala nga kayong maraming mga ama; sapagka't kay Cristo Jesus ipinanganak ko kayo sa pamamagitan ng evangelio.
Ipinamamanhik ko nga sa inyo, na kayo'y maging mga tagatulad sa akin.
Dahil dito'y aking sinugo sa inyo si Timoteo, na aking minamahal at tapat na anak sa Panginoon, na siya ang sa inyo'y magpapaalaala ng aking mga daang nanga kay Cristo, gaya ng itinuturo ko saan mang dako sa bawa't iglesia.
Ang mga iba nga'y nangagpapalalo, na waring hindi na ako mapapariyan sa inyo.
Nguni't ako'y paririyan agad sa inyo, kung loloobin ng Panginoon; at aking aalamin, hindi ang salita ng nangagpapalalo, kundi ang kapangyarihan.
Sapagka't ang kaharian ng Dios ay hindi sa salita, kundi sa kapangyarihan.
Anong ibig ninyo? paririyan baga ako sa inyo na may panghampas, o sa pagibig, at sa espiritu ng kahinahunan?
Sa kasalukuya'y nababalita na sa inyo'y may pakikiapid, at ang ganyang pakikiapid ay wala kahit sa mga Gentil, na isa sa inyo'y nagaari ng asawa ng kaniyang ama.
At kayo'y mga mapagpalalo, at hindi kayo bagkus nangalumbay, upang maalis sa gitna ninyo ang gumagawa ng gawang ito.
Sapagka't ako sa katotohanan, bagama't wala sa harapan ninyo sa katawan nguni't ako'y nasa harapan ninyo sa espiritu, akin ngang hinatulan na ang gumagawa ng bagay na ito, na tulad sa ako'y nahaharap,
Sa pangalan ng ating Panginoong Jesus, nang nangagkakatipon kayo, at ang aking espiritu, na taglay ang kapangyarihan ng ating Panginoong Jesus,
Upang ang ganyan ay ibigay kay Satanas sa ikawawasak ng kaniyang laman, upang ang espiritu ay maligtas sa araw ng Panginoong Jesus.
Hindi mabuti ang inyong pagmamapuri. Hindi baga ninyo nalalaman na ang kaunting lebadura ay nagpapakumbo sa buong limpak?
Alisin ninyo ang lumang lebadura, upang kayo'y maging bagong limpak, na tulad sa kayo'y walang lebadura. Sapagka't ang kordero ng ating paskua ay naihain na, sa makatuwid baga'y si Cristo.
Kaya nga ipangilin natin ang pista, hindi sa lumang lebadura, ni sa lebadura man ng masamang akala at ng kasamaan, kundi sa tinapay na walang lebadura ng pagtatapat at ng katotohanan.
Isinulat ko sa inyo sa aking sulat na huwag kayong makisama sa mga mapakiapid;
Tunay nga hindi ang ibig sabihin ay sa mga mapakiapid sa sanglibutang ito, o sa mga masasakim at mga manglulupig, o sa mga mananamba sa diosdiosan; sapagka't kung gayo'y kinakailangang magsialis kayo sa sanglibutan:
Datapuwa't sinusulatan ko nga kayo, na huwag kayong makisama sa kanino mang tinatawag na kapatid, kung siya'y mapakiapid, o masakim, o mananamba sa diosdiosan, o mapagtungayaw, o manglalasing, o manglulupig; sa gayo'y huwag man lamang kayong makisalo.
Sapagka't ano sa akin ang humatol sa nangasa labas? Hindi baga kayo nagsisihatol sa nangasa loob?
Datapuwa't sa nangasa labas ay Dios ang humahatol. Alisin nga ninyo sa inyo ang masamang tao.
Nangangahas baga ang sinoman sa inyo, kung mayroong anomang bagay laban sa iba, na siya'y magsakdal sa harapan ng mga liko, at hindi sa harapan ng mga banal?
O hindi baga ninyo nalalaman na ang mga banal ay magsisihatol sa sanglibutan? at kung ang sanglibutan ay hahatulan ninyo, hindi kaya baga dapat magsihatol kayo sa mga bagay na pinakamaliit?
Hindi baga ninyo nalalaman na ating hahatulan ang mga anghel? gaano pa kaya ang mga bagay na nauukol sa buhay na ito?
Kung kayo nga'y mayroong usapin na mga bagay na nauukol sa buhay na ito, ilalagay baga ninyo upang magsihatol ang mga taong walang halaga sa iglesia?
Sinasabi ko ito upang mangahiya kayo. Ano, diyata't wala baga sa inyo na isa mang marunong na makapagpapayo sa kaniyang mga kapatid,
Kundi ang kapatid ay nakikipagusapin laban sa kapatid, at ito'y sa harapan ng mga hindi nagsisipanampalataya?
Ngayon nga, tunay na isang pagkukulang sa inyo ang kayo-kayo'y magkaroon ng mga usapin. Bakit hindi bagkus ninyong tiisin ang mga kalikuan? bakit hindi bagkus kayo'y padaya?
Nguni't kayo rin ang mga nagsisigawa ng kalikuan, at nangagdaraya, at ito'y sa mga kapatid ninyo.
O hindi baga ninyo nalalaman na ang mga liko ay hindi magsisipagmana ng kaharian ng Dios? Huwag kayong padaya: kahit ang mga mapakiapid, ni ang mga mananamba sa diosdiosan, ni ang mga mangangalunya, ni ang mga nangbababae, ni ang mga mapakiapid sa kapuwa lalake.
Ni ang mga magnanakaw, ni ang mga masasakim, ni ang mga manglalasing, ni ang mga mapagtungayaw, ni ang mga manglulupid, ay hindi mangagmamana ng kaharian ng Dios.
At ganyan ang mga ilan sa inyo: nguni't nangahugasan na kayo, nguni't binanal na kayo, nguni't inaring-ganap na kayo sa pangalan ng Panginoong Jesucristo, at sa Espiritu ng ating Dios.
Ang lahat ng mga bagay sa akin ay matuwid; nguni't hindi ang lahat ay nararapat. Ang lahat ng mga bagay sa akin ay matuwid; nguni't hindi ako pasasakop sa kapangyarihan ng anoman.
Ang mga pagkain ay sa tiyan, at ang tiyan ay sa mga pagkain: nguni't kapuwa iwawasak ng Dios yaon at ang mga ito. Datapuwa't ang katawan ay hindi sa pakikiapid, kundi sa Panginoon; at ang Panginoon ay sa katawan:
At muling binuhay ng Dios ang Panginoon, at muling bubuhayin naman tayo sa pamamagitan ng kaniyang kapangyarihan.
Hindi baga ninyo nalalaman na ang inyong mga katawan ay mga sangkap ni Cristo? kukunin ko nga baga ang mga sangkap ni Cristo, at gagawin kong mga sangkap ng isang patutot? Huwag nawang mangyari.
O hindi baga ninyo nalalaman na ang nakikisama sa patutot, ay kaisang katawan niya? sapagka't sinasabi niya, Ang dalawa ay magiging isang laman.
Nguni't ang nakikisama sa Panginoon, ay kaisang espiritu niya.
Magsitakas kayo sa pakikiapid. Lahat ng kasalanang gawin ng mga tao ay nangasa labas ng katawan; nguni't ang gumagawa ng pakikiapid ay nagkakasala laban sa kaniyang sariling katawan.
O hindi baga ninyo nalalaman na ang inyong katawan ay templo ng Espiritu Santo na nasa inyo, na tinanggap ninyo sa Dios? at hindi kayo sa inyong sarili;
Sapagka't kayo'y binili sa halaga: luwalhatiin nga ninyo ng inyong katawan ang Dios.
At tungkol sa mga bagay na isinulat ninyo sa akin: Mabuti sa lalake ay huwag humipo sa babae.
Datapuwa't dahil sa mga pakikiapid, ang bawa't lalake ay magkaroon ng kaniyang sariling asawa, at bawa't babae ay magkaroon ng kaniyang sariling asawa.
Ibigay ng lalake sa asawa ang sa kaniya'y nararapat: at gayon din naman ang babae sa asawa.
Ang babae ay walang kapangyarihan sa kaniyang sariling katawan, kundi ang asawa: at gayon din naman ang lalake ay walang kapangyarihan sa kaniyang sariling katawan, kundi ang asawa.
Huwag magpigil ang isa't isa, maliban kung pagkasunduan sa ilang panahon, upang kayo'y mamalagi sa pananalangin, at muling kayo'y magsama, baka kayo'y tuksuhin ni Satanas dahil sa inyong kawalan ng pagpipigil.
Nguni't ito'y sinasabi ko na parang payo, hindi sa utos.
Kaya't ibig ko sana, na ang lahat ng tao ay maging gaya ko. Nguni't ang bawa't tao'y mayroong kanikaniyang kaloob na mula sa Dios, ang isa'y ayon sa paraang ito, at ang iba'y ayon sa paraan yaon.
Datapuwa't sinasabi ko sa mga walang asawa, at sa mga babaing bao, Mabuti sa kanila kung sila'y magsipanatiling gayon sa makatuwid baga'y gaya ko.
Nguni't kung sila'y hindi makapagpigil, ay magsipagasawa: sapagka't magaling ang magasawa kay sa mangagningas ang pita.
Datapuwa't sa mga may asawa ay aking ipinaguutos, Nguni't hindi ako, kundi ang Panginoon, na ang babae ay huwag humiwalay sa kaniyang asawa.
(Datapuwa't kung siya'y humiwalay, ay manatiling walang asawa, o kaya'y makipagkasundo sa kaniyang asawa); at huwag hiwalayan ng lalake ang kaniyang asawa.
Datapuwa't sa iba, ay ako ang nagsasabi, hindi ang Panginoon: Kung ang sinomang kapatid na lalake ay may asawang hindi sumasampalataya, at kung kalooban niyang makipamahay sa kaniya, ay huwag niyang hiwalayan.
At ang babaing may asawang hindi sumasampalataya, at kalooban niyang hiwalayan ang kaniyang asawa.
Sapagka't ang lalaking hindi sumasampalataya ay nagiging banal sa pamamagitan ng kaniyang asawa, at ang babaing hindi sumasampalataya ay nagiging banal sa pamamagitan ng kaniyang asawa: sa ibang paraa'y ang inyong mga anak ay nangagkaroon ng kapintasan; nguni't ngayo'y mga banal.
Gayon ma'y kung humiwalay ang hindi sumasampalataya, ay bayaan siyang humiwalay: ang kapatid na lalake o kapatid na babae ay hindi natatali sa mga ganitong bagay: kundi sa kapayapaan kayo tinawag ng Dios.
Sapagka't paanong malalaman mo, Oh babae, kung maililigtas mo ang iyong asawa? o paanong malalaman mo, Oh lalake, kung maililigtas mo ang iyong asawa?
Ayon nga lamang sa ipinamahagi ng Panginoon sa bawa't isa, at ayon sa pagkatawag ng Dios sa bawa't isa, ay gayon siya lumakad. At gayon ang iniuutos ko sa lahat ng mga iglesia.
Tinawag baga ang sinomang taong tuli na? huwag siyang maging di tuli. Tinawag baga ang sinomang di tuli? huwag siyang maging tuli.
Ang pagtutuli ay walang anoman, at ang di pagtutuli ay walang anoman; kundi ang pagtupad sa mga utos ng Dios.
Bayaang ang bawa't isa'y manatili doon sa pagkatawag na itinawag sa kaniya.
Ikaw baga'y alipin ng ikaw ay tinawag? huwag kang magalaala: kung maaaring ikaw ay maging malaya, ay pagsikapan mo ng maging laya.
Sapagka't ang tinawag sa Panginoon nang siya'y alipin, ay malaya sa Panginoon: gayon din naman ang tinawag nang siya'y malaya, ay alipin ni Cristo.
Sa halaga kayo'y binili; huwag kayong maging alipin ng mga tao.
Mga kapatid, bayaang ang bawa't isa'y manatili sa Dios sa kalagayang itinawag sa kaniya.
Ngayon, tungkol sa mga dalaga ay wala akong utos ng Panginoon: nguni't ibinibigay ko ang aking pasiya, na tulad sa nagkamit ng habag ng Panginoon upang mapagkatiwalaan.
Inaakala ko ngang mabuti ito dahil sa kasalukuyang kahapisan, sa makatuwid baga'y mabuti ngang ang tao'y manatili ng ayon sa kaniyang kalagayan.
Natatali ka ba sa asawa? huwag mong pagsikapang ikaw ay makakalag. Ikaw baga'y kalag sa asawa? huwag kang humanap ng asawa.
Nguni't kung ikaw ay magasawa, ay hindi ka nagkakasala; at kung ang isang dalaga ay magasawa, ay hindi siya nagkakasala. Datapuwa't ang mga gayon ay magkakaroon ng kahirapan sa laman: at ibig ko sanang kayo'y iligtas.
Nguni't sinasabi ko ito, mga kapatid, ang panahon ay pinaikli, upang mula ngayon ang mga lalaking may asawa ay maging mga tulad sa wala;
At ang mga nagsisiiyak, ay maging tulad sa mga hindi nagsisiiyak; at ang nangagagalak, ay maging tulad sa hindi nangagagalak; at ang mga nagsisibili, ay maging tulad sa mga walang inaari;
At ang mga nagsisigamit ng sanlibutan, ay maging tulad sa hindi nangagpapakalabis ng paggamit: sapagka't ang kaasalan ng sanglibutang ito ay lumilipas.
Datapuwa't ang ibig ko ay mawalan kayo ng kabalisahan. Ang walang asawa ay nagsusumakit sa mga bagay ng Panginoon, kung paanong makalulugod sa Panginoon:
Nguni't ang may asawa ay nagsusumakit sa mga bagay ng sanglibutan, kung paanong makalulugod sa kaniyang asawa,
At nagkakabahagi ang isipan. Gayon din naman ang babaing walang asawa at ang dalaga, ay nagsusumakit sa mga bagay ng Panginoon, upang siya'y maging banal sa katawan at sa espiritu man; nguni't ang babaing may asawa ay nagsusumakit sa mga bagay ng sanglibutan, kung paanong makalulugod sa kaniyang asawa.
At ito'y sinasabi ko sa inyong sariling kapakinabangan; hindi upang alisin ko ang inyong kalayaan, kundi dahil sa bagay na nararapat, at upang kayo'y makapaglingkod sa Panginoon nang walang abala.
Nguni't kung iniisip ng sinomang lalake na hindi siya gumagawa ng marapat sa kaniyang anak na dalaga, kung ito'y sumapit na sa kaniyang katamtamang gulang, at kung kailangan ay sundin niya ang kaniyang maibigan, hindi siya nagkakasala; bayaang mangagasawa sila.
Subali't ang nananatiling matibay sa kaniyang puso, na walang kailangan, kundi may kapangyarihan tungkol sa kaniyang sariling kalooban, at pinasiyahan sa kaniyang sariling puso na ingatan ang kaniyang sariling anak na dalaga, ay mabuti ang gagawin.
Kaya nga ang nagpapahintulot sa kaniyang anak na dalaga na magasawa ay gumagawa ng mabuti; at ang hindi nagpapahintulot na siya'y magasawa ay gumagawa ng lalong mabuti.
Ang babaing may asawa ay natatalian samantalang nabubuhay ang kaniyang asawa; nguni't kung patay na ang kaniyang asawa, ay may kalayaan siyang makapagasawa sa kanino mang ibig niya; sa kalooban lamang ng Panginoon.
Nguni't lalong maligaya siya kung manatili ng ayon sa kaniyang kalagayan, ayon sa aking akala: at iniisip ko na ako'y may Espiritu rin naman ng Dios.
Ngayon tungkol sa mga bagay na inihain sa mga diosdiosan: Nalalaman natin na tayong lahat ay may kaalaman. Ang kaalaman ay nagpapalalo, nguni't ang pagibig ay nagpapatibay.
Kung ang sinoman ay nagaakala na siya'y may nalalamang anoman, ay wala pang nalalaman gaya ng nararapat niyang malaman;
Datapuwa't kung ang sinoman ay umiibig sa Dios, ay kilala niya ang gayon.
Tungkol nga sa pagkain ng mga bagay na inihain sa mga diosdiosan, nalalaman natin na ang diosdiosan ay walang kabuluhan sa sanglibutan, at walang Dios liban sa iisa.
Sapagka't bagama't mayroong mga tinawag na mga dios, maging sa langit o maging sa lupa; gaya nang may maraming mga dios at maraming mga panginoon;
Nguni't sa ganang atin ay may isang Dios lamang, ang Ama, na buhat sa kaniya ang lahat ng mga bagay, at tayo'y sa kaniya; at isa lamang Panginoon, si Jecrucristo, na sa pamamagitan niya ang lahat ng mga bagay, at tayo'y sa pamamagitan niya.
Gayon ma'y wala sa lahat ng mga tao ang kaalamang iyan: kundi ang ilan na hanggang ngayon ay nangamimihasa sa diosdiosan, ay kumakain na parang isang bagay na inihain sa diosdiosan; at ang kanilang budhi palibhasa'y mahina ay nangahahawa.
Datapuwa't ang pagkain ay hindi magtataguyod sa atin sa Dios: ni hindi tayo masama, kung tayo'y magsikain; ni hindi tayo lalong mabuti, kung tayo'y magsikain.
Datapuwa't magsipagingat kayo baka sa anomang paraan ang inyong kalayaang ito ay maging katitisuran sa mahihina.
Sapagka't kung makita ng sinomang ikaw na may kaalaman na nakikisalo sa pagkain at templo ng diosdiosan, hindi baga titibay ang kaniyang budhi, kung siya'y mahina, upang kumain ng mga bagay na inihain sa mga diosdiosan?
Sapagka't sa pamamagitan ng iyong kaalaman ay napapahamak ang mahina, ang kapatid na dahil sa kaniya'y namatay si Cristo.
At sa ganitong pagkakasala laban sa mga kapatid, at sa pagkakasugat ng kaniyang budhi kung ito'y mahina, ay nangagkakasala kayo laban kay Cristo.
Kaya, kung ang pagkain ay nakapagpapatisod sa aking kapatid, kailan man ay hindi ako kakain ng lamang-kati, upang ako'y huwag makapagpatisod sa aking kapatid.
Hindi baga ako'y malaya? hindi baga ako'y apostol? hindi ko baga nakita si Jesus na Panginoon natin? hindi baga kayo'y gawa ko sa Panginoon?
Kung sa iba'y hindi ako apostol, sa inyo man lamang ako'y gayon; sapagka't ang tatak ng aking pagkaapostol ay kayo sa Panginoon.
Ito ang aking pagsasanggalang sa mga nagsisiyasat sa akin.
Wala baga kaming matuwid na magsikain at magsiinom?
Wala baga kaming matuwid na magsipagsama ng isang asawa na sumasampalataya, gaya ng iba't ibang mga apostol, at ng mga kapatid ng Panginoon, at ni Cefas?
O ako baga lamang at si Bernabe ang walang matuwid na magsitigil ng paggawa?
Sinong kawal ang magpakailan pa man ay naglilingkod sa kaniyang sariling gugol? sino ang nagtatanim ng isang ubasan, at hindi kumakain ng bunga niyaon? o sino ang nagpapakain sa kawan, at hindi kumakain ng gatas ng kawan?
Ang mga ito bagay sinasalita ko ayon sa kaugalian lamang ng mga tao? o di baga sinasabi rin naman ang gayon ng kautusan?
Sapagka't nasusulat sa kautusan ni Moises, Huwag mong lalagyan ng busal ang baka pagka gumigiik. Ang mga baka baga ay iniingatan ng Dios,
O sinasabi kayang tunay ito dahil sa atin? Oo, dahil sa atin ito sinulat: sapagka't ang nagsasaka ay dapat magsaka sa pagasa, at ang gumigiik, ay sa pagasa na makakabahagi.
Kung ipaghasik namin kayo ng mga bagay na ayon sa espiritu, malaking bagay baga na aming anihin ang inyong mga bagay na ayon sa laman?
Kung ang iba ay mayroon sa inyong matuwid, hindi baga lalo pa kami? Gayon ma'y hindi namin ginamit ang matuwid na ito; kundi aming tinitiis ang lahat ng mga bagay, upang huwag kaming makahadlang sa evangelio ni Cristo.
Hindi baga ninyo nalalaman na ang mga nagsisipangasiwa sa mga bagay na banal, ay nagsisikain ng mga bagay na ukol sa templo, at ang mga nagsisipaglingkod sa dambana ay mga kabahagi ng dambana?
Gayon din naman ipinagutos ng Panginoon na ang mga nagsisipangaral ng evangelio.
Nguni't ako'y hindi gumamit ng anoman sa mga bagay na ito: at hindi ko sinusulat ang mga bagay na ito upang gawin ang gayon sa akin; sapagka't mabuti pa sa akin ang mamatay, kay sa pawalang kabuluhan ninoman ang aking karangalan.
Sapagka't kung ipinangangaral ko ang evangelio, ay wala akong sukat ipagmapuri; sapagka't ang pangangailangan ay iniatang sa akin; sapagka't sa aba ko kung hindi ko ipangaral ang evangelio.
Sapagka't kung ito'y gawin ko sa aking sariling kalooban, ay may ganting-pala ako: nguni't kung hindi sa aking sariling kalooban, ay mayroon akong isang pamamahala na ipinagkatiwala sa akin.
Ano nga kaya ang aking ganting-pala? Na pagka ipinangangaral ko ang evangelio, ay ang evangelio ay maging walang bayad, upang huwag kong gamiting lubos ang aking karapatan sa evangelio.
Sapagka't bagaman ako ay malaya sa lahat ng mga tao, ay napaalipin ako sa lahat, upang ako'y makahikayat ng lalong marami.
At sa mga Judio, ako'y nagaring tulad sa Judio, upang mahikayat ko ang mga Judio; sa mga nasa ilalim ng kautusan ay gaya ng nasa ilalim ng kautusan, bagaman wala ako sa ilalim ng kautusan upang mahikayat ang mga nasa ilalim ng kautusan;
Sa mga walang kautusan, ay tulad sa walang kautusan, bagama't hindi ako walang kautusan sa Dios, kundi nasa ilalim ng kautusan ni Cristo, upang mahikayat ko ang mga walang kautusan.
Sa mga mahihina ako'y nagaring mahina, upang mahikayat ko ang mahihina: sa lahat ng mga bagay ay nakibagay ako sa lahat ng mga tao, upang sa lahat ng mga paraan ay mailigtas ko ang ilan.
At ginawa ko ang lahat ng mga bagay dahil sa evangelio, upang ako'y makasamang makabahagi nito.
Hindi baga ninyo nalalaman na ang mga nagsisitakbo sa takbuhan ay tumatakbong lahat, nguni't iisa lamang ang tumatanggap ng ganting-pala? Magsitakbo kayo ng gayon; upang magsipagtamo kayo.
At ang bawa't tao na nakikipaglaban sa mga palaruan ay mapagpigil sa lahat ng mga bagay. Ginagawa nga nila ito upang magsipagtamo ng isang putong na may pagkasira; nguni't tayo'y niyaong walang pagkasira.
Ako nga'y tumatakbo sa ganitong paraan, na hindi gaya ng nagsasapalaran; sa ganito rin ako'y sumusuntok, na hindi gaya ng sumusuntok sa hangin:
Nguni't hinahampas ko ang aking katawan, at aking sinusupil: baka sakaling sa anomang paraan, pagkapangaral ko sa iba, ay ako rin ang itakuwil.
Sapagka't hindi ko ibig mga kapatid, na di ninyo maalaman, na ang ating mga magulang ay nangapasa ilalim ng alapaap, at ang lahat ay nagsitawid sa dagat;
At lahat ng nangabautismuhan kay Moises sa alapaap at sa dagat;
At lahat ay nagsikain ng isang pagkain ding ayon sa espiritu;
At lahat ay nagsiinom ng isang inumin ding ayon sa espiritu; sapagka't nagsiinom sa batong ayon sa espiritu na sumunod sa kanila: at ang batong yaon ay si Cristo.
Bagaman ang karamihan sa kanila ay hindi nakalugod sa Dios; sapagka't sila'y ibinuwal sa ilang.
Ang mga bagay na ito nga'y pawang naging mga halimbawa sa atin, upang huwag tayong magsipagnasa ng mga bagay na masama na gaya naman nila na nagsipagnasa.
Ni huwag din naman kayong magpasamba sa mga diosdiosan, gaya niyaong ilan sa kanila; ayon sa nasusulat, Naupo ang bayan upang magsikain at magsiinom, at nagsitindig upang magsipagsayaw.
Ni huwag din naman tayong makiapid, na gaya ng ilan sa kanila na nangakiapid, at ang nangabuwal sa isang araw ay dalawangpu at tatlong libo.
Ni huwag din naman nating tuksuhin ang Panginoon, na gaya ng pagkatukso ng ilan sa kanila, at nangapahamak sa pamamagitan ng mga ahas.
Ni huwag din kayong mangagbulongbulungan, na gaya ng ilan sa kanila na nangagbulungan, at nangapahamak sa pamamagitan ng mga mangwawasak.
Ang mga bagay na ito nga'y nangyari sa kanila na pinakahalimbawa; at pawang nangasulat sa pagpapaalaala sa atin, na mga dinatnan ng katapusan ng mga panahon.
Kaya't ang may akalang siya'y nakatayo, magingat na baka mabuwal.
Hindi dumating sa inyo ang anomang tukso kundi yaong matitiis ng tao: datapuwa't tapat ang Dios, na hindi niya itutulot na kayo'y tuksuhin ng higit sa inyong makakaya; kundi kalakip din ng tukso ay gagawin naman ang paraan ng pagilag, upang ito'y inyong matiis.
Kaya, mga minamahal ko, magsitakas kayo sa pagsamba sa diosdiosan.
Akoy nagsasalitang tulad sa marurunong; hatulan ninyo ang sinasabi ko.
Ang saro ng pagpapala na ating pinagpapala, hindi baga siyang pakikipagkaisa ng dugo ni Cristo? Ang tinapay na ating pinagpuputolputol, hindi baga siyang pakikipagkaisa ng katawan ni Cristo?
Bagaman tayo'y marami, ay iisa lamang tinapay, iisang katawan: sapagka't tayong lahat ay nakikibahagi sa isa lamang tinapay.
Tingnan ninyo ang Israel na ayon sa laman: ang mga nagsisikain baga ng mga hain ay wala kayang pakikipagkaisa sa dambana?
Ano ang aking sinasabi? na ang hain baga sa mga diosdiosan ay may kabuluhan? o ang diosdiosan ay may kabuluhan?
Subali't sinasabi ko, na ang mga bagay na inihahain ng mga Gentil, ay kanilang inihahain sa mga demonio, at hindi sa Dios: at di ko ibig na kayo'y mangagkaroon ng pakikipagkaisa sa mga demonio.
Hindi ninyo maiinuman ang saro ng Panginoon, at ang saro ng mga demonio: kayo'y hindi maaaring makisalo sa dulang ng Panginoon, at sa dulang ng mga demonio.
O minumungkahi baga natin sa paninibugho ang Panginoon? tayo baga'y lalong malakas kay sa kaniya?
Lahat ng mga bagay ay matuwid; nguni't hindi ang lahat ng mga bagay ay nararapat. Lahat ng mga bagay ay matuwid; nguni't hindi ang lahat ng mga bagay ay makapagpapatibay.
Huwag hanapin ninoman ang sa kaniyang sarili, kundi ang ikabubuti ng kapuwa.
Lahat ng ipinagbibili sa pamilihan ay kanin ninyo, at huwag kayong magsipagtanong ng anoman dahilan sa budhi;
Sapagka't ang lupa ay sa Panginoon at ang kabuuan ng naririto.
Kung kayoy aanyayahan ng isang hindi sumasampalataya, at ibig ninyong pumaroon; ang anomang ihain sa inyo ay kanin ninyo, at huwag kayong magsipagtanong ng anoman dahilan sa budhi.
Datapuwa't kung sa inyo'y may magsabi, Ito'y inihandog na hain, ay huwag ninyong kanin, dahilan doon sa nagpahayag at dahilan sa budhi;
Budhi, sinasabi ko, hindi ang inyong sarili, kundi ang sa iba; sapagka't bakit hahatulan ang kalayaan ng budhi ng iba?
Kung nakikisalo ako na may pagpapasalamat, bakit ako'y aalipustain ng dahil sa aking ipinagpapasalamat?
Kaya kung kayo'y nagsisikain man, o nagsisiinom man o anoman ang inyong ginagawa, gawin ninyo ang lahat sa ikaluluwalhati ng Dios.
Huwag kayong magbigay ng dahilang ikatitisod, sa mga Judio man, sa mga Griego man, o sa iglesia man ng Dios:
Na gaya ko din naman, na sa lahat ng mga bagay ay nagbibigay lugod ako sa lahat ng mga tao, na hindi ko hinahanap ang aking sariling kapakinabangan, kundi ang sa marami, upang sila'y mangaligtas.
Maging taga tulad kayo sa akin, na gaya ko naman kay Cristo.
Kayo'y aking pinupuri nga, na sa lahat ng mga bagay ay naaalaala ninyo ako, at iniingatan ninyong matibay ang mga turo, na gaya ng ibinigay ko sa inyo.
Datapuwa't ibig ko na inyong maalaman, na ang pangulo ng bawa't lalake ay si Cristo, at ang pangulo ng babae ay ang lalake, at ang pangulo ni Cristo ay ang Dios.
Ang bawa't lalaking nanalangin, o nanghuhula na may takip ang ulo, ay niwawalan ng puri ang kaniyang ulo.
Datapuwa't ang bawa't babaing nananalangin o nanghuhula na walang lambong ang kaniyang ulo, niwawalan ng puri ang kaniyang ulo; sapagka't gaya rin ng kung kaniyang inahitan.
Sapagka't kung ang babae ay walang lambong, ay pagupit naman; nguni't kung kahiyahiya sa babae ang pagupit o paahit, ay maglambong siya.
Sapagka't katotohanang ang lalake ay hindi dapat maglambong sa kaniyang ulo, palibhasa'y larawan siya at kaluwalhatian ng Dios: nguni't ang babae ay siyang kaluwalhatian ng lalake.
Sapagka't ang lalake ay hindi sa babae; kundi ang babae ay sa lalake:
Sapagka't hindi nilalang ang lalake dahil sa babae; kundi ang babae dahil sa lalake;
Dahil dito'y nararapat na ang babae ay magkaroon sa kaniyang ulo ng tanda ng kapamahalaan, dahil sa mga anghel.
Gayon man, ang babae ay di maaaring walang lalake at ang lalake ay di maaaring walang babae, sa Panginoon.
Sapagka't kung paanong ang babae ay sa lalake, gayon din naman ang lalake ay sa pamamagitan ng babae; datapuwa't ang lahat ng mga bagay ay sa Dios.
Hatulan ninyo sa inyo-inyong sarili: nararapat baga na manalangin ang babae sa Dios nang walang lambong?
Hindi baga ang katalagahan din ang nagtuturo sa inyo, na kung may mahabang buhok ang lalake, ay mahalay sa kaniya?
Datapuwa't kung ang babae ang may mahabang buhok, ay isang kapurihan niya; sapagka't ang buhok sa kaniya'y ibinigay na pangtakip.
Datapuwa't kung tila mapagtunggali ang sinoman, walang gayong ugali kami, ni ang iglesia man ng Dios.
Datapuwa't sa pagtatagubilin sa inyo nito, ay hindi ko kayo pinupuri, sapagka't kayo'y nangagkakatipon hindi sa lalong mabuti kundi sa lalong masama.
Sapagka't unauna'y nababalitaan ko na kung nangagkakatipon kayo sa iglesia, ay mayroon sa inyong mga pagkakabahabahagi; at may kaunting paniniwala ako.
Sapagka't tunay na sa inyo'y mayroong mga hidwang pananampalataya, upang yaong mga napatunayan na ay mangahayag sa inyo.
Kung kayo nga ay nangagkakatipon, ay hindi kayo maaaring magsikain ng hapunan ng Panginoon;
Sapagka't sa inyong pagkain, ang bawa't isa'y kumukuha ng kanikaniyang sariling hapunan na nagpapauna sa iba; at ang isa ay gutom, at ang iba'y lasing.
Ano, wala baga kayong mga bahay na inyong makakanan at maiinuman? o niwawalang halaga ninyo ang iglesia ng Dios, at hinihiya ninyo ang mga wala ng anoman? Ano ang aking sasabihin sa inyo? Kayo baga'y aking pupurihin? Sa bagay na ito ay hindi ko kayo pinupuri.
Sapagka't tinanggap ko sa Panginoon ang ibinibigay ko naman sa inyo; na ang Panginoong Jesus nang gabing siya'y ipagkanulo ay dumampot ng tinapay;
At nang siya'y makapagpasalamat, ay kaniyang pinagputolputol, at sinabi, Ito'y aking katawan na pinagputolputol dahil sa inyo: gawin ninyo ito sa pagaalaala sa akin.
At gayon din naman hinawakan ang saro pagkatapos makahapon, na sinasabi, Ang sarong ito'y siyang bagong tipan sa aking dugo: gawin ninyo ito sa tuwing kayo'y magsisiinom, sa pagaalaala sa akin.
Sapagka't sa tuwing kanin ninyo ang tinapay na ito, at inuman ninyo ang saro, ay inihahayag ninyo ang pagkamatay ng Panginoon hanggang sa dumating siya.
Kaya't ang sinomang kumain ng tinapay, o uminom sa saro ng Panginoon, na di nararapat, ay magkakasala sa katawan at dugo ng Pangioon.
Datapuwa't siyasain ng tao ang kaniyang sarili at saka kumain ng tinapay, at uminom sa saro.
Sapagka't kumakain at umiinom, ay kumakain at umiinom ng hatol sa kaniyang sarili, kung hindi niya kinikilala ang katawan ng Panginoon.
Dahil dito'y marami sa inyo ang mahihina at mga masasaktin, at hindi kakaunti ang nangatutulog.
Datapuwa't kung ating kilalanin ang ating sarili, ay hindi tayo hahatulan.
Datapuwa't kung tayo'y hinahatulan, ay pinarurusahan tayo ng Panginoon, upang huwag tayong mahatulang kasama ng sanglibutan.
Dahil dito, mga kapatid ko, kung kayo'y mangagsasalosalo sa pagkain, ay mangaghintayan kayo.
Kung ang sinoman ay magutom, kumain siya sa bahay; upang ang inyong pagsasalosalo ay huwag maging sa paghatol. At ang iba ay aking aayusin pagpariyan ko.
Ngayong tungkol sa mga kaloob na ayon sa espiritu, mga kapatid, ay hindi ko ibig na hindi kayo makaalam.
Nalalaman ninyo na nang kayo'y mga Gentil pa, ay iniligaw kayo sa mga piping diosdiosan, sa alin mang paraang pagkahatid sa inyo.
Kaya't ipinatatalastas ko sa inyo, na wala sinomang nagsasalita sa pamamagitan ng Espiritu ng Dios ay nagsasabi, Si Jesus ay itinakwil; at wala sinoman ay makapagsasabi, Si Jesus ay Panginoon kundi sa pamamagitan ng Espiritu Santo.
Ngayo'y may iba't ibang mga kaloob, datapuwa't iisang Espiritu.
At may iba't ibang mga pangangasiwa, datapuwa't iisang Panginoon.
At may iba't ibang paggawa, datapuwa't iisang Dios na gumagawa ng lahat ng mga bagay sa lahat.
Datapuwa't sa bawa't isa ay ibinibigay ang paghahayag ng Espiritu, upang pakinabangan naman.
Sapagka't sa isa, sa pamamagitan ng Espiritu ay ibinibigay ang salita ng karunungan; at sa iba'y ang salita ng kaalaman ayon din sa Espiritu:
Sa iba'y ang pananampalataya, sa gayon ding Espiritu; at sa iba'y ang mga kaloob na pagpapagaling, sa iisang Espiritu.
At sa iba'y ang mga paggawa ng mga himala; at sa iba'y hula; at sa iba'yang pagkilala sa mga espiritu; at sa iba'y ang iba't ibang wika; at sa iba'y ang pagpapaliwanag ng mga wika.
Datapuwa't ang lahat ng ito ay ginagawa ng isa at ng gayon ding Espiritu, na binabahagi sa bawa't isa ayon sa kaniyang ibig.
Sapagka't kung paanong ang katawan ay iisa, at mayroong maraming mga sangkap, at ang lahat ng mga sangkap ng katawan, bagama't marami, ay iisang katawan; gayon din naman si Cristo.
Sapagka't sa isang Espiritu ay binabautismuhan tayong lahat sa isang katawan, maging tayo'y Judio o Griego, maging mga alipin o mga laya; at tayong lahat ay pinainom sa isang Espiritu.
Sapagka't ang katawan ay hindi iisang sangkap, kundi marami.
Kung sasabihin ng paa, Sapagka't hindi ako kamay, ay hindi ako sa katawan; hindi nga dahil dito'y hindi sa katawan.
At kung sasabihin ng tainga, Sapagka't hindi ako mata, ay hindi ako sa katawan; hindi nga dahil dito'y hindi sa katawan.
Kung ang buong katawan ay pawang mata, saan naroon ang pakinig? Kung ang lahat ay pawang pakinig, saan naroon ang pangamoy.
Datapuwa't ngayo'y inilagay ng Dios ang bawa't isa sa mga sangkap ng katawan, ayon sa kaniyang minagaling.
At kung ang lahat nga'y pawang isang sangkap, saan naroroon ang katawan?
Datapuwa't maraming mga sangkap nga, nguni't iisa ang katawan.
At hindi makapagsasabi ang mata sa kamay, Hindi kita kinakailangan: at hindi rin ang ulo sa mga paa, Hindi ko kayo kailangan.
Hindi, kundi lalo pang kailangan yaong mga sangkap ng katawan na wari'y lalong mahihina:
At yaong mga sangkap ng katawan, na inaakala nating kakaunti ang kapurihan, sa mga ito ipinagkakaloob natin ang lalong saganang papuri; at ang mga sangkap nating mga pangit ay siyang may lalong saganang kagandahan;
Yamang ang mga sangkap nating magaganda ay walang kailangan: datapuwa't hinusay ng Dios ang katawan na binigyan ng lalong saganang puri yaong sangkap na may kakulangan;
Upang huwag magkaroon ng pagkakabahabahagi sa katawan; kundi ang mga sangkap ay mangagkaroon ng makasing-isang pagiingat sa isa't isa.
At kung ang isang sangkap ay nagdaramdam, ang lahat ng mga sangkap ay nangagdaramdam na kasama niya; o kung ang isang sangkap ay nagkakapuri, ang lahat ng mga sangkap ay nangagagalak na kasama niya.
Kayo nga ang katawan ni Cristo, at bawa't isa'y samasamang mga sangkap niya.
At ang Dios ay naglagay ng ilan sa iglesia, una-una'y mga apostol, ikalawa'y mga propeta, ikatlo'y mga guro, saka mga himala, saka mga kaloob na pagpapagaling, mga pagtulong, mga pamamahala, at iba't ibang mga wika.
Lahat baga'y mga apostol? lahat baga'y mga propeta? lahat baga'y mga guro? lahat baga'y mga manggagawa ng mga himala?
May mga kaloob na pagpapagaling baga ang lahat? nangagsasalita baga ang lahat ng mga wika? lahat baga ay nangagpapaliwanag?
Datapuwa't maningas ninyong nasain, ang lalong dakilang mga kaloob. At itinuturo ko sa inyo ang isang daang kagalinggalingan.
Kung ako'y magsalita ng mga wika ng mga tao at ng mga anghel, datapuwa't wala akong pagibig, ay ako'y naging tanso na tumutunog, o batingaw na umaalingawngaw.
At kung magkaroon ako ng kaloob na panghuhula, at maalaman ko ang lahat ng mga hiwaga at ang lahat ng mga kaalaman; at kung magkaroon ako ng buong pananampalataya, na ano pa't mapalipat ko ang mga bundok, datapuwa't wala akong pagibig, ay wala akong kabuluhan.
At kung ipagkaloob ko ang lahat ng aking mga tinatangkilik upang ipakain sa mga dukha, at kung ibigay ko ang aking katawan upang sunugin, datapuwa't wala akong pagibig ay walang pakikinabangin sa akin.
Ang pagibig ay mapagpahinuhod, at magandang-loob; ang pagibig ay hindi nananaghili; ang pagibig ay hindi nagmamapuri, hindi mapagpalalo.
Hindi naguugaling mahalay, hindi hinahanap ang kaniyang sarili, hindi nayayamot, hidi inaalumana ang masama;
Hindi nagagalak sa kalikuan, kundi nakikigalak sa katotohanan;
Lahat ay binabata, lahat ay pinaniniwalaan, lahat ay inaasahan, lahat ay tinitiis.
Ang pagibig ay hindi nagkukulang kailan man: kahit maging mga hula, ay mangatatapos; maging mga wika, ay titigil: maging kaalaman, ay mawawala.
Sapagka't nangakakakilala tayo ng bahagya, at nanganghuhula tayo ng bahagya;
Datapuwa't kung dumating ang sakdal, ang bahagya ay matatapos.
Nang ako'y bata pa, ay nagsasalita akong gaya ng bata, nagdaramdam akong gaya ng bata, nagiisip akong gaya ng bata: ngayong maganap ang aking pagkatao, ay iniwan ko na ang mga bagay ng pagkabata.
Sapagka't ngayo'y malabo tayong nakakikita sa isang salaman; nguni't pagkatapos ay makikita natin sa mukhaan: ngayo'y nakikilala ko ng bahagya, nguni't pagkatapos ay makikilala ko ng gaya ng pagkakilala sa akin.
Datapuwa't ngayo'y nanatili ang tatlong ito: ang pananampalataya, ang pagasa, at ang pagibig; nguni't ang pinakadakila sa mga ito ay ang pagibig.
Sundin ninyo ang pagibig; gayon ma'y maningas ninyong pakanasain ang mga kaloob na ayon sa espiritu, nguni't lalo na ang kayo'y mangakapanghula.
Sapagka't ang nagsasalita ng wika ay hindi sa mga tao nagsasalita, kundi sa Dios; sapagka't walang nakauunawa sa kaniya; kundi sa espiritu ay nagsasalita ng mga hiwaga.
Datapuwa't ang nanghuhula ay nagsasalita sa mga tao sa ikatitibay, at sa ikapangangaral, at sa ikaaaliw.
Ang nagsasalita ng wika, ay nagpapatibay sa sarili; nguni't ang naghuhula ay nagpapatibay sa iglesia.
Ibig ko sanang kayong lahat ay mangagsalita ng mga wikaa, datapuwa't lalo na ang kayo'y magsipanghula: at lalong dakila ang nanghuhula kay sa nagsasalita ng mga wika, maliban na kung siya'y magpapaliwanag upang ang iglesia ay mapagtibay.
Ngayon nga, mga kapatid, kung ako'y pumariyan sa inyo na nagsasalita ng mga wika, anong inyong pakikinabangin sa akin, maliban na kung kayo'y pagsalitaan ko sa pamamagitan ng pahayag, o ng kaalaman, o ng panghuhula, o ng aral?
Kahit ang mga bagay na walang buhay, na nagsisitunog, maging plauta, o alpa, kundi bigyan ng pagkakaiba ang mga tunog, paanong malalaman kung ano ang tinutugtog sa plauta o sa alpa?
Sapagka't kung ang pakakak ay tumunog na walang katiyakan, sino ang hahanda sa pakikipabaka?
Gayon din naman kayo, kung hindi ipinangungusap ninyo ng dila ang mga salitang madaling maunawaan, paanong matatalos ang inyong sinasalita? sapagka't sa hangin kayo magsisipagsalita.
Halimbawa, mayroon ngang iba't ibang mga tinig sa sanglibutan, at walang di may kahulugan.
Kung hindi ko nga nalalaman ang kahulugan ng tinig, magiging barbaro ako sa nagsasalita, at ang nagsasalita ay magiging barbaro sa akin.
Gayon din naman kayo, na yamang kayo'y mapagsikap sa mga kaloob na ayon sa espiritu ay pagsikapan ninyong kayo'y magsisagana sa ikatitibay ng iglesia.
Kaya't ang nagsasalita ng wika ay managalangin na siya'y makapagpaliwanag.
Sapagka't kung ako'y nananalangin sa wika, ay nananalangin ang aking espiritu, datapuwa't ang aking pagiisip ay hindi namumunga.
Ano nga ito? Mananalangin ako sa espiritu, at aawit din naman ako sa pagiisip: aawit ako sa espiritu, at aawit din naman ako sa pagiisip.
Sa ibang paraan, kung ikaw ay nagpupuri sa espiritu, ang nasa kalagayan ng di marunong, paanong siya'y makapagsasabi ng Siya nawa, sa iyong pagpapasalamat, palibhasa'y hindi nalalaman ang inyong sinasabi?
Sapagka't ikaw sa katotohanan ay nagpapasalamat kang mabuti, datapuwa't ang iba'y hindi napapagtibay.
Nagpapasalamat ako sa Dios, na ako'y nagsasalita ng mga wika nahigit kay sa inyong lahat:
Nguni't sa iglesia ibig ko pang magsalita ng limang salita ng aking pagiisip, upang makapagturo ako naman sa iba, kay sa magsalita ng sangpung libong salita sa wika.
Mga kapatid, huwag kayong mangagpakabata sa pagiisip; gayon ma'y sa kahalayan ay mangagpakasanggol kayo, datapuwa't sa pagiisip kayo'y mangagpakatao.
Sa kautusan ay nasusulat, sa pamamagitan ng mga taong may iba't ibang wika, at sa pamamagitan ng mga labi ng mga taga ibang lupa ay magsasalita ako, sa bayang ito: at gayon ma'y hindi ako pakikinggan nila, ang sabi ng Panginoon.
Kaya nga ang mga wika ay pinaka tanda, hindi sa mga nagsisisampalataya; kundi sa mga hindi nagsisisampalataya; nguni't ang panghuhula ay hindi sa mga hindi nagsisisampalataya, kundi sa mga nagsisisampalataya.
Kung ang buong iglesia nga'y magkatipon sa isang dako at lahat ay mangagsalita ng mga wika, at mangagsipasok ang mga hindi marurunong o hindi mga nagsisisampalataya, hindi baga nila sasabihing kayo'y mga ulol?
Datapuwa't kung ang lahat ay nagsisipanghula, at may pumasok na isang hindi sumasampalataya, o hindi marunong, mahihikayat siya ng lahat, masisiyasat siya ng lahat;
Ang mga lihim ng kaniyang puso ay nahahayag; at sa gayo'y magpapatirapa siya at sasamba sa Dios, na sasabihing tunay ngang ang Dios ay nasa gitna ninyo.
Ano nga ito mga kapatid? Pagka kayo'y nangagkakatipon ang bawa't isa sa inyo'y may isang awit, may isang aral, may isang pahayag, may wika, may isang pagpapaliwanag. Gawin ninyo ang lahat ng mga bagay sa ikatitibay.
Kung nagsasalita ang sinoman ng wika, maging dalawa, o huwag higit sa tatlo, at sunodsunod; at ang isa'y magpapaliwanag:
Datapuwa't kung walang tagapagpaliwanag ay tumahimik siya sa iglesia; at siya'y magsalita sa kaniyang sarili, at sa Dios.
At ang dalawa o tatlo sa mga propeta ay magsipagsalita, at ang mga iba'y mangagsiyasat.
Datapuwa't kung may ipinahayag na anoman sa isang nauupo, ay tumahimik ang nauuna.
Sapagka't kayong lahat ay makapanghuhulang isa-isa, upang ang lahat ay mangatuto, at ang lahat ay maaralan;
At ang mga espiritu ng mga propeta ay nasasakupan ng mga propeta;
Sapagka't ang Dios ay hindi Dios ng kaguluhan, kundi ng kapayapaan. Gaya nsa lahat ng mga iglesia ng mga banal,
Ang mga babae ay magsitahimik sa mga iglesia: sapagka't sila'y walang kapahintulutang mangagsalita; kundi sila'y pasakop, gaya naman ng sinasabi ng kautusan.
At kung ibig nilang maalaman ang anomang bagay, magtanong sila sa kanilang asawa sa bahay; sapagka't mahalay na ang isang babae ay magsalita sa iglesia.
Ano? lumabas baga mula sa inyo ang salita ng Dios? o sa inyo lamang dumating?
Kung iniisip ninoman na siya'y propeta, o ayon sa espiritu, ay kilalanin niya ang mga bagay na sa inyo'y isinusulat ko, na pawang utos ng Panginoon.
Datapuwa't kung ang sinoman ay mangmang, ay manatili siyang mangmang.
Kaya nga, mga kapatid ko, maningas na pakanasain ninyong makapanghula, at huwag ninyong ipagbawal ang magsalita ng mga wika.
Datapuwa't gawin ninyong may karapatan at may kaayusan ang lahat ng mga bagay.
Ngayo'y ipinatatalastas ko sa inyo, mga kapatid, ang evangelio na sa inyo'y aking ipinangaral, na inyo namang tinanggap, na siya naman ninyong pinananatilihan,
Sa pamamagitan naman nito'y ligtas kayo kung matiyaga ninyong iingatan ang salitang ipinangaral ko sa inyo, maliban na kung kayo'y nagsipanampalataya nang walang kabuluhan.
Sapagka't ibinigay ko sa inyo una sa lahat, ang akin namang tinanggap: na si Cristo ay namatay dahil sa ating mga kasalanan, ayon sa mga kasulatan,
At siya'y inilibing; at siya'y muling binuhay nang ikatlong araw ayon sa mga kasulatan;
At siya'y napakita kay Cefas, at saka sa labingdalawa;
Pagkatapos ay napakita sa mahigit na limang daang kapatid na paminsan, na ang karamihan sa mga ito'y nangabubuhay hanggang ngayon, datapuwa't ang mga iba'y nangatulog na;
Saka napakita kay Santiago; at saka sa lahat ng mga apostol;
At sa kahulihulihan, tulad sa isang ipinanganak sa di kapanahunan, ay napakita naman siya sa akin.
Ako nga ang pinakamaiit sa mga apostol, at hindi ako karapatdapat na tawaging apostol, sapagka't pinagusig ko ang iglesia ng Dios.
Datapuwa't sa pamamagitan ng biyaya ng Dios, ako nga'y ako; at ang kaniyang biyaya na ibinigay sa akin ay hindi nawawalan ng kabuluhan; bagkus ako'y malabis na nagpagal kay sa kanilang lahat: bagaman hindi ako, kundi ang biyaya ng Dios na sumasa akin.
Maging ako nga o sila, ay gayon ang aming ipinangangaral, at gayon ang inyong sinampalatayanan.
Kung si Cristo nga'y ipinangangaral na siya'y muling binuhay sa mga patay, bakit ang ilan sa inyo ay nagsisipagsabi na walang pagkabuhay na maguli ng mga patay?
Datapuwa't kung walang pagkabuhay na maguli ng mga patay, ay hindi rin nga muling binuhay si Cristo:
At kung si Cristo'y hindi mulang binuhay, ay walang kabuluhan nga ang aming pangangaral, wala rin namang kabuluhan ang inyong pananampalataya.
Oo, at kami ay nasusumpungang mga saksing bulaan tungkol sa Dios; sapagka't aming sinasaksihan tungkol sa Dios, na kaniyang muling binuhay si Cristo; na hindi siyang muling nabuhay, kung tunay nga ang mga patay ay hindi muling binubuhay.
Sapagka't kung hindi muling binubuhay ang mga patay, ay hindi rin nga muling binuhay si Cristo:
At kung si Cristo ay hindi muling binuhay, ang inyong pananampalataya ay walang kabuluhan kayo'y nasa inyong mga kasalanan pa.
Kung gayon nga, ang mga nangatutulog din naman kay Cristo ay pawang nangapahamak.
Kung sa buhay lamang na ito tayo nagsisiasa kay Cristo, ay tayo sa lahat ng mga tao ang lalong kahabaghabag.
Datapuwa't si Cristo nga'y muling binuhay sa mga patay na siya ay naging pangunahing bunga ng nangatutulog.
Sapagka't yamang sa pamamagitan ng tao'y dumating ang kamatayan, sa pamamagitan din naman ng tao'y dumating ang pagkabuhay na maguli sa mga patay.
Sapagka't kung paanong kay Adam ang lahat ay nangamamatay, gayon din naman kayo kay Cristo ang lahat ay bubuhayin.
Datapuwa't ang bawa't isa'y sa kaniyang sariling katayuan; si Cristo ang pangunahing bunga; pagkatapos ay ang mga kay Cristo, sa kaniyang pagparito.
Kung magkagayo'y darating ang wakas, pagka ibibigay na niya ang kaharian sa Dios, sa makatuwid baga'y sa Ama; pagka lilipulin na niya ang lahat ng paghahari, at lahat ng kapamahalaan at kapangyarihan.
Sapagka't kinakailangang siya'y maghari hanggang mailagay niya sa ilalim ng kaniyang mga talampakan ang lahat niyang mga kaaway.
Ang kahulihulihang kaaway na lilipulin ay ang kamatayan.
Sapagka't kaniyang pinasuko ang lahat ng mga bagay sa ilalim ng kaniyang paa. Dapatuwa't kung sinasabi, ang lahat ng mga bagay ay pinasuko, ay maliwanag na itinangi yaong nagpasuko ng lahat ng mga bagay sa kaniya.
At kung ang lahat ng mga bagay ay mapasuko na sa kaniya, kung magkagayo'y ang Anak rin ay pasusukuin naman sa nagpasuko ng lahat ng mga bagay sa kaniya, upang ang Dios ay maging lahat sa lahat.
Sa ibang paraan, anong gagawin ng mga binabautismuhan dahil sa mga patay? Kung ang mga patay ay tunay na hindi muling binubuhay, bakit nga sila'y binabautismuhan dahil sa kanila?
Bakit baga naman tayo'y nanganganib bawa't oras?
Ipinahahayag ko alangalang sa ikaluluwalhati ninyo, mga kapatid, na taglay ko kay Cristo Jesus na Panginoon natin na araw-araw ay nasa panganib ng kamatayan ako.
Kung ako'y nakipagbaka sa Efeso laban sa mga ganid, ayon sa kaugalian ng tao, ano ang pakikinabangin ko? Kung ang mga patay ay hindi muling binubuhay, magsikain at magsiinom tayo yamang bukas tayo'y mangamamatay.
Huwag kayong padaya: Ang masasamang kasama ay sumisira ng magagandang ugali.
Gumising kayo ng ayon sa katuwiran, at huwag mangagkasala; sapagka't may mga ibang walang pagkakilala sa Dios: sinasabi ko ito upang kayo'y kilusin sa kahihiyan.
Datapuwa't sasabihin ng iba, Paanong muling bubuhayin ang mga patay? at anong anyo ng katawan ang iparirito nila?
Ikaw na mangmang, ang inyong inihahasik ay hindi binubuhay maliban na kung mamatay:
At ang iyong inihahasik ay hindi mo inihahasik ang katawang lilitaw, kundi ang butil laman, na marahil ay trigo, o ibang bagay;
Datapuwa't ang Dios ay nagbibigay ng katawan dito ayon sa kaniyang minagaling, at sa bawa't binhi ay ang sariling katawan.
Hindi ang lahat ng laman ay magkasing-isang laman: kundi may laman sa mga tao, at ibang laman sa mga hayop, at ibang laman sa mga ibon, at ibang laman sa mga isda.
Mayroon namang mga katawang ukol sa langit, at mga katawang ukol sa lupa: datapuwa't iba ang kaluwalhatian ng ukol sa langit, at iba ang ukol sa lupa.
Iba ang kaluwalhatian ng araw, at iba ang kaluwalhatian ng buwan, at iba ang kaluwalhatian ng mga bituin; sapagka't ang isang bituin ay naiiba sa ibang bituin sa kaluwalhatian.
Gayon din naman ang pagkabuhay na maguli ng mga patay. Itinatanim na may kasiraan; binubuhay na maguli na walang kasiraan;
Itinatanim na may pagkasiphayo; binubuhay na maguli na may kaluwalhatian: itinatanim na may kahinaan; binubuhay na maguli na may kapangyarihan:
Itinatanim na may katawang ukol sa lupa; binubuhay na maguli na katawang ukol sa espiritu. Kung may katawang ukol sa lupa ay may katawang ukol sa espiritu naman.
Gayon din naman nasusulat, Ang unang taong si Adam ay naging kaluluwang buhay. Ang huling Adam ay naging espiritung nagbibigay buhay.
Bagaman ang ukol sa espiritu ay hindi siyang una, kundi ang ukol sa lupa: pagkatapos ang ukol sa espiritu.
Ang unang tao ay taga lupa na ukol sa lupa: ang ikalawang tao ay taga langit.
Kung ano ang ukol sa lupa, ay gayon din naman silang mga taga lupa: at kung ano ang ukol sa langit ay gayon din naman silang taga langit.
At kung paanong tinaglay natin ang larawang ukol sa lupa, ay tataglayin din naman natin ang larawang ukol sa langit.
Sinasabi ko nga ito, mga kapatid, na ang laman at ang dugo ay hindi makapagmamana ng kaharian ng Dios; ni ang kasiraan ay magmamana ng walang kasiraan.
Narito, sinasaysay ko sa inyo ang isang hiwaga: hindi tayong lahat ay mangatutulog, nguni't tayong lahat ay babaguhin,
Sa isang sangdali, sa isang kisapmata, sa huling pagtunog ng pakakak: sapagka't tutunog ang pakakak, at ang mga patay ay mangabubuhay na maguli na walang kasiraan, at tayo'y babaguhin.
Sapagka't kinakailangan na itong may kasiraan ay magbihis ng walang kasiraan, at itong may kamatayan ay magbihis ng walang kamatayan.
Datapuwa't pagka itong may kasiraan ay mabihisan ng walang kasiraan, at itong may kamatayan ay mabihisan ng walang kamatayan, kung magkagayon ay mangyayari ang wikang nasusulat, Nilamon ng pagtatagumpay ang kamatayan.
Saan naroon, Oh kamatayan, ang iyong pagtatagumpay? Saan naroon, Oh kamatayan ang iyong tibo?
Ang tibo ng kamatayan ay ang kasalanan; at ang kapangyarihan ng kasalanan ay ang kautusan:
Datapuwa't salamat sa Dios, na nagbibigay sa atin ng pagtatagumpay sa pamamagitan ng ating Panginoong Jesucristo.
Kaya nga, mga kapatid kong minamahal, kayo'y magsitatag, huwag makilos, na laging sumasagana sa gawa ng Panginoon, yamang nalalaman ninyo na hindi walang kabuluhan ang inyong gawa sa Panginoon.
Ngayon tungkol sa ambagan sa mga banal, ay gawin din naman ninyong gaya ng iniutos ko sa mga iglesia ng Galacia.
Tuwing unang araw ng sanglinggo ang bawa't isa sa inyo ay magbukod na magsimpan, ayon sa kaniyang iginiginhawa, upang huwag nang gumawa ng mga ambagan sa pagpariyan ko.
At pagdating ko, ang sinomang mamagalingin, ay sila ang aking susuguin na may mga sulat upang makapagdala ng inyong abuloy sa Jerusalem:
At kung nararapat na ako naman ay pumaroon, sila'y isasama ko.
Nguni't ako'y paririyan sa inyo, pagkaraan ko sa Macedonia; sapagka't magdaraan ako sa Macedonia;
Datapuwa't marahil ako'y matitira sa inyo o makikisama sa inyo sa taginaw, upang ako'y tulungan ninyo sa aking paglalakbay saan man ako pumaroon.
Sapagka't hidi ko ibig na kayo'y makita ngayon sa paglalakbay; sapagka't inaasahan kong ako'y makikisama sa inyong kaunting panahon, kung itutulot ng Panginoon.
Datapuwa't ako'y titigil sa Efeso hanggang sa Pentecostes;
Sapagka't sa akin ay nabuksan ang pintuang malaki at mapapakinabangan, at marami ang mga kaaway.
Ngayon kung dumating si Timoteo, ingatan ninyo na siya'y mapasainyo na walang pangamba; sapagka't ginagawa niya ang gawain ng Panginoon, na gaya ko rin naman:
Sinoman nga ay huwag humamak sa kaniya. Kundi tulungan ninyo siyang payapa sa kaniyang paglalakbay, upang siya'y makaparito sa akin: sapagka't inaasahan ko siya'y kasama ng mga kapatid.
Nguni't tungkol sa kapatid na si Apolos, ay ipinamanhik ko sa kaniyang malabis na siya'y pumariyan sa inyong kasama ng mga kapatid: at sa anomang paraan ay hindi niya kalooban na pumariyan ngayon; nguni't paririyan pagkaroon niya ng panahon.
Magsipagingat kayo, mangagpakatibay kayo sa pananampalataya, kayo'y mangagpakalalake, kayo'y mangagpakalakas.
Gawin ninyo sa pagibig ang lahat ninyong ginagawa.
Ipinamamanhik ko nga sa inyo, mga kapatid (nalalaman ninyo na ang sangbahayan ni Estefanas ay siyang pangunahing bunga ng Acaya, at nangagsitalaga sa paglilingkod sa mga banal).
Na kayo'y pasakop naman sa mga gayon, at sa bawa't tumutulong sa gawa at nagpapagal.
At ikinagagalak ko ang pagdating ni Estefanas at ni Fortunato at ni Acaico: sapagka't ang kakulangan ninyo ay pinunan nila.
Sapagka't inaliw nila ang aking espiritu at ang inyo: magsikilala nga kayo sa mga gayon.
Binabati kayo ng mga iglesia sa Asia. Kayo'y binabating malabis sa Panginoon ni Aquila at ni Prisca pati ng iglesiang nasa kanilang bahay.
Binabati kayo ng lahat ng mga kapatid. Kayo'y mangagbatian ng halik na banal.
Ang bati ko, ni Pablo na sinulat ng aking sariling kamay.
Kung ang sinoman ay hindi umiibig sa Panginoon, ay maging takuwil siya. Maranatha.
Ang biyaya ng Panginoong Jesucristo ay sumainyo nawa.
Ang aking pagibig kay Cristo Jesus ay sumainyo nawang lahat. Siya nawa.
