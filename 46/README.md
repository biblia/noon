# 1 Mga Taga Corinto

|reference     | start | end |
|:---------------|----:|----:|
|1 Corinto 1:1-31|    1|   31|
|1 Corinto 2:1-16|   32|   47|
|1 Corinto 3:1-23|   48|   70|
|1 Corinto 4:1-21|   71|   91|
|1 Corinto 5:1-13|   92|  104|
|1 Corinto 6:1-20|  105|  124|
|1 Corinto 7:1-40|  125|  164|
|1 Corinto 8:1-13|  165|  177|
|1 Corinto 9:1-27|  165|  204|
|1 Corinto 10:1-33| 205|  237|
|1 Corinto 11:1-34| 238|  271|
|1 Corinto 12:1-31| 272|  302|
|1 Corinto 13:1-13| 303|  315|
|1 Corinto 14:1-40| 316|  355|
|1 Corinto 15:1-58| 356|  413|
|1 Corinto 16:1-24| 414|  437|

